local Keys = {
  ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
  ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
  ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
  ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
  ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
  ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
  ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
  ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
  ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

local isDoingAction = false

function getDryerState(business, propLetter)
  local businessConfig = Config.BusinessSetup.Money_Wash

  local dryer_off = string.format("dryer%s_off", propLetter)
  local dryer_on = string.format("dryer%s_on", propLetter)
  local dryer_open = string.format("dryer%s_open", propLetter)

  if IsInteriorPropEnabled(businessConfig.interiorID, dryer_off) then
    return "Off"
  elseif IsInteriorPropEnabled(businessConfig.interiorID, dryer_on) then
    return "On"
  elseif IsInteriorPropEnabled(businessConfig.interiorID, dryer_open) then
    return "Open"
  end
end


--Action Blocking
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(1)

    if isDoingAction then
      DisableControlAction(0, Keys['F1'], true) -- Disable phone
      DisableControlAction(0, Keys['F2'], true) -- Inventory
      DisableControlAction(0, Keys['F3'], true) -- Animations
      DisableControlAction(0, Keys['F6'], true) -- Job
      DisableControlAction(0, Keys['Enter'], true)
      DisableControlAction(0, Keys['E'], true)
    else
      Citizen.Wait(500)
    end
  end
end)


RegisterNetEvent('esx_business:moneyPrintStart')
AddEventHandler('esx_business:moneyPrintStart', function(business, hostThisInstance)
  local scenario = 'PROP_HUMAN_BUM_BIN'
  TaskStartScenarioInPlace(PlayerPedId(), scenario, 0, false)

  local coords = GetEntityCoords(PlayerPedId())
  local interiorID = GetInteriorAtCoords(coords)
  local sleepTime = nil
  if not IsInteriorPropEnabled(interiorID, 'counterfeit_upgrade_equip_no_prod') then
    sleepTime = (1000 * 60) * Config.TimeToPrintBasic
  else
    sleepTime = (1000 * 60) * Config.TimeToPrintUpgrade
  end
  --local sleepTime = 3000

  local ped = PlayerPedId()
  local oldCoords = GetEntityCoords(ped)
  local duration = Config.TimeToAnim
  local timer = 0
  local failed = false

  while IsPedUsingScenario(ped,scenario) == false do
    Wait(0)
  end

  while failed == false do
    isDoingAction = true
    timer = timer + 1
    local coords = GetEntityCoords(ped)
    local isDead = IsEntityDead(ped)
    local movedAway = GetDistanceBetweenCoords(oldCoords, coords.x, coords.y, coords.z, true) > Config.MarkerSize.x
    local cancelledAnim = IsPedUsingScenario(ped,scenario) == false

    if isDead or movedAway or cancelledAnim then
      isDoingAction = false
      ESX.ShowNotification("The action was cancelled")
      failed = true
      ClearPedTasksImmediately(PlayerPedId())
      break
    end

    if timer == duration then
      isDoingAction = false
      ClearPedTasksImmediately(PlayerPedId())
      printerType = nil
      if IsInteriorPropEnabled(interiorID, 'counterfeit_standard_equip_no_prod') then
        printerType = 'standard'
      elseif IsInteriorPropEnabled(interiorID, 'counterfeit_upgrade_equip_no_prod') then
        printerType = 'upgrade'
      end

      TriggerServerEvent('esx_business:moneyPrintDone', business, hostThisInstance, sleepTime, printerType)
      break
    end

    Wait(1000)
  end
end)

RegisterNetEvent('esx_business:produceDirtyMoneyStart')
AddEventHandler('esx_business:produceDirtyMoneyStart', function(business, hostThisInstance)
  local scenario = 'PROP_HUMAN_BUM_BIN'
  TaskStartScenarioInPlace(PlayerPedId(), scenario, 0, false)

  local coords = GetEntityCoords(PlayerPedId())
  local interiorID = GetInteriorAtCoords(coords)

  local ped = PlayerPedId()
  local oldCoords = GetEntityCoords(ped)
  local duration = Config.TimeToAnim
  local timer = 0
  local failed = false

  while IsPedUsingScenario(ped,scenario) == false do
    Wait(0)
  end

  while failed == false do
    isDoingAction = true
    timer = timer + 1
    local coords = GetEntityCoords(ped)
    local isDead = IsEntityDead(ped)
    local movedAway = GetDistanceBetweenCoords(oldCoords, coords.x, coords.y, coords.z, true) > Config.MarkerSize.x
    local cancelledAnim = IsPedUsingScenario(ped,scenario) == false

    if isDead or movedAway or cancelledAnim then
      isDoingAction = false
      ESX.ShowNotification("The action was cancelled")
      failed = true
      ClearPedTasksImmediately(PlayerPedId())
      break
    end

    if timer == duration then
      isDoingAction = false
      ClearPedTasksImmediately(PlayerPedId())
      TriggerServerEvent('esx_business:produceDirtyMoneyDone', business, hostThisInstance, sleepTime)
      break
    end

    Wait(1000)
  end
end)


RegisterNetEvent('esx_business:takePrintedMoneyStart')
AddEventHandler('esx_business:takePrintedMoneyStart', function(business, hostThisInstance)
  local ped = PlayerPedId()
  local oldCoords = GetEntityCoords(ped)
  local duration = Config.TimeToAnim
  local timer = 0
  local failed = false

  local coords = GetEntityCoords(PlayerPedId())
  local interiorID = GetInteriorAtCoords(coords)
  local scenario = 'PROP_HUMAN_BUM_BIN'
  TaskStartScenarioInPlace(PlayerPedId(), scenario, 0, false)

  while IsPedUsingScenario(ped,scenario) == false do
    Wait(0)
  end

  while failed == false do
    isDoingAction = true
    timer = timer + 1
    local coords = GetEntityCoords(ped)
    local isDead = IsEntityDead(ped)
    local movedAway = GetDistanceBetweenCoords(oldCoords, coords.x, coords.y, coords.z, true) > Config.MarkerSize.x
    local cancelledAnim = IsPedUsingScenario(ped,scenario) == false

    if isDead or movedAway or cancelledAnim then
      isDoingAction = false
      ESX.ShowNotification("The action was cancelled")
      failed = true
      ClearPedTasksImmediately(PlayerPedId())
      break
    end

    if timer == duration then
      isDoingAction = false
      ClearPedTasksImmediately(PlayerPedId())

      local amount = nil
      if IsInteriorPropEnabled(interiorID, 'counterfeit_standard_equip') then
        amount = Config.GiveCounterfeitNotesBasic
      elseif IsInteriorPropEnabled(interiorID, 'counterfeit_upgrade_equip') then
        amount = Config.GiveCounterfeitNotesUpgrade
      end

      TriggerServerEvent('esx_business:takePrintedMoneyDone', business, hostThisInstance, amount)
      break
    end

    Wait(1000)
  end
end)


RegisterNetEvent('esx_business:moneyWashStart')
AddEventHandler('esx_business:moneyWashStart', function(business, hostThisInstance, dryerVar, quantity)
  local scenario = 'PROP_HUMAN_BUM_BIN'
  TaskStartScenarioInPlace(PlayerPedId(), scenario, 0, false)

  local ped = PlayerPedId()
  local oldCoords = GetEntityCoords(ped)
  local duration = Config.TimeToAnim
  local timer = 0
  local failed = false

  while IsPedUsingScenario(ped,scenario) == false do
    Wait(0)
  end

  while failed == false do
    isDoingAction = true
    timer = timer + 1
    local coords = GetEntityCoords(ped)
    local isDead = IsEntityDead(ped)
    local movedAway = GetDistanceBetweenCoords(oldCoords, coords.x, coords.y, coords.z, true) > Config.MarkerSize.x
    local cancelledAnim = IsPedUsingScenario(ped,scenario) == false

    if isDead or movedAway or cancelledAnim then
      isDoingAction = false
      ESX.ShowNotification("The action was cancelled")
      failed = true
      break
    end

    if timer == duration then
      isDoingAction = false
      ClearPedTasksImmediately(PlayerPedId())
      TriggerServerEvent('esx_business:moneyWashDone', business, hostThisInstance, dryerVar, quantity)
      break
    end

    Wait(1000)
  end
end)


RegisterNetEvent('esx_business:takeWashedMoneyStart')
AddEventHandler('esx_business:takeWashedMoneyStart', function(business, hostThisInstance, dryerVar)
  TriggerEvent('esx:showNotification', _U('action_start'))

  local scenario = 'PROP_HUMAN_BUM_BIN'
  TaskStartScenarioInPlace(PlayerPedId(), scenario, 0, false)

  local ped = PlayerPedId()
  local oldCoords = GetEntityCoords(ped)
  local duration = Config.TimeToAnim
  local timer = 0
  local failed = false

  while IsPedUsingScenario(ped,scenario) == false do
    Wait(0)
  end

  while failed == false do
    isDoingAction = true
    timer = timer + 1
    local coords = GetEntityCoords(ped)
    local isDead = IsEntityDead(ped)
    local movedAway = GetDistanceBetweenCoords(oldCoords, coords.x, coords.y, coords.z, true) > Config.MarkerSize.x
    local cancelledAnim = IsPedUsingScenario(ped,scenario) == false

    if isDead or movedAway or cancelledAnim then
      isDoingAction = false
      ESX.ShowNotification("The action was cancelled")
      failed = true
      ClearPedTasksImmediately(PlayerPedId())
      break
    end

    if timer == duration then
      isDoingAction = false
      ClearPedTasksImmediately(PlayerPedId())
      TriggerServerEvent('esx_business:takeWashedMoneyDone', business, hostThisInstance, dryerVar)
      break
    end

    Wait(1000)
  end
end)
