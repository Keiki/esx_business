local Keys = {
  ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
  ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
  ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
  ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
  ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
  ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
  ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
  ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
  ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

ESX                           = nil
local PlayerData              = nil
local GUI                     = {}
GUI.Time                      = 0
local OwnedBusinesses         = {}
local Blips                   = {}
local CurrentBusiness         = nil
local CurrentBusinessOwner    = nil
local LastBusiness            = nil
local LastPart                = nil
local HasAlreadyEnteredMarker = false
local CurrentAction           = nil
local CurrentActionMsg        = ''
local CurrentActionData       = {}
local FirstSpawn              = true
local HasChest                = false


Citizen.CreateThread(function()
  while ESX == nil do
    TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
    Citizen.Wait(0)
  end
end)



function DrawSub(text, time)
  ClearPrints()
  SetTextEntry_2('STRING')
  AddTextComponentString(text)
  DrawSubtitleTimed(time, 1)
end


function splitString(inputstr, sep)
  if sep == nil then
    sep = "%s"
  end
  local t={} ; i=1
  for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
    t[i] = str
    i = i + 1
  end
  return t
end


function CreateBlips()
  for i=1, #Config.Businesses, 1 do
    local business = Config.Businesses[i]

    if business.entering ~= nil then
      Blips[business.name] = AddBlipForCoord(business.entering.x, business.entering.y, business.entering.z)

      ESX.TriggerServerCallback('esx_business:getBusinessIsOwned', function(isOwned) -- checks if the business is owned by anyone
        local sprite, text, category = nil, nil, nil

        if isOwned == 0 then
          sprite = 375
          text = 'free_prop'
          category = 10
        else
          sprite = 374
          text = 'business'
          category = 11
        end

        SetBlipSprite(Blips[business.name], sprite)
        SetBlipDisplay(Blips[business.name], 4)
        SetBlipScale(Blips[business.name], 1.0)
        SetBlipAsShortRange(Blips[business.name], true)
        SetBlipCategory(Blips[business.name], category)

        BeginTextCommandSetBlipName("STRING")
        AddTextComponentString(_U(text)) -- why is this nil?
        EndTextCommandSetBlipName(Blips[business.name])
      end, business.name)
    end
  end
end


RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
  PlayerData.job = job
end)

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
  PlayerData = xPlayer

  ESX.TriggerServerCallback('esx_business:getBusinesses', function(businesses)
    Config.Businesses = businesses
    CreateBlips()
  end)

  ESX.TriggerServerCallback('esx_business:getOwnedBusinesses', function(ownedBusinesses)
    for i=1, #ownedBusinesses, 1 do
      SetBusinessOwned(ownedBusinesses[i], true)
    end
  end)
end)

-- only used when script is restarting mid-session
RegisterNetEvent('esx_business:sendBusinesses')
AddEventHandler('esx_business:sendBusinesses', function(businesses)
  Config.Businesses = businesses
  CreateBlips()

  ESX.TriggerServerCallback('esx_business:getOwnedBusinesses', function(ownedBusinesses)
    for i=1, #ownedBusinesses, 1 do
      SetBusinessOwned(ownedBusinesses[i], true)
    end
  end)
end)


function GetBusinesses()
  return Config.Businesses
end


function GetBusiness(name)
  for i=1, #Config.Businesses, 1 do
    if Config.Businesses[i].name == name then
      return Config.Businesses[i]
    end
  end
end


function EnterBusiness(name, owner)
  local business       = GetBusiness(name)
  local playerPed      = GetPlayerPed(-1)
  CurrentBusiness      = business
  CurrentBusinessOwner = owner

  for i=1, #Config.Businesses, 1 do
    if Config.Businesses[i].name ~= name then
      Config.Businesses[i].disabled = true
    end
  end

  TriggerServerEvent('esx_business:saveLastBusiness', name)

  Citizen.CreateThread(function()
    DoScreenFadeOut(800)

    while not IsScreenFadedOut() do
      Citizen.Wait(0)
    end

    ESX.TriggerServerCallback('esx_business:getBusinessType', function(type)
      local businessConfig = Config.BusinessSetup[type]
      local zones = businessConfig.zones
      local interiorID = businessConfig.interiorID

      SetEntityCoords(playerPed, zones.inside.x,  zones.inside.y,  zones.inside.z)
      while not IsInteriorReady(interiorID) do
        Citizen.Wait(0)
      end
      DoScreenFadeIn(2000)
    end, name)

    ESX.TriggerServerCallback('esx_business:getBusinessLabel', function(label)
      DrawSub(label, 5000) --attempt to index nil value business
    end, business.name)
  end)
end


function ExitBusiness(name)
  local business  = GetBusiness(name)
  local playerPed = GetPlayerPed(-1)
  local outside   = nil
  CurrentBusiness = nil

  outside = business.outside

  TriggerServerEvent('esx_business:deleteLastBusiness')

  Citizen.CreateThread(function()
    DoScreenFadeOut(800)

    while not IsScreenFadedOut() do
      Citizen.Wait(0)
    end

    SetEntityCoords(playerPed, outside.x,  outside.y,  outside.z)

    for i=1, #Config.Businesses, 1 do
      Config.Businesses[i].disabled = false
    end

    DoScreenFadeIn(2000)
  end)
end


function SetBusinessOwned(name, owned)
  local business     = GetBusiness(name)
  local entering     = business.entering --attempt to index nil value business
  local enteringName = business.name

  if owned then
    OwnedBusinesses[name] = true

    RemoveBlip(Blips[enteringName])

    Blips[enteringName] = AddBlipForCoord(entering.x,  entering.y,  entering.z)

    SetBlipSprite(Blips[enteringName], 374)
    SetBlipAsShortRange(Blips[enteringName], true)
    SetBlipCategory(Blips[enteringName], 11)

    BeginTextCommandSetBlipName("STRING")
    AddTextComponentString(_U('business'))
    EndTextCommandSetBlipName(Blips[enteringName])

    --[[ESX.TriggerServerCallback('esx_business:getBusinessType', function(type)
      local sprite, text, type = nil, nil, nil

      if type == "Weed_Farm" then
        sprite = 496
      elseif type == "Cocaine_Lockup" then
        sprite = 497
      elseif type == "Meth_Lab" then
        sprite = 499
      elseif type == "Doc_Forge" then
        sprite = 498
      elseif type == "Money_Wash" then
        sprite = 500
      else
        sprite = 374
      end

      text = string.lower(type)

      SetBlipSprite(Blips[business.name], sprite)
      SetBlipDisplay(Blips[business.name], 4)
      SetBlipScale(Blips[business.name], 1.0)
      SetBlipAsShortRange(Blips[business.name], true)

      BeginTextCommandSetBlipName("STRING")
      AAddTextComponentString(_U(text))
      EndTextCommandSetBlipName(Blips[business.name])
    end, business.name)]]
  else
    OwnedBusinesses[name] = nil

    local found = false

    for k,v in pairs(OwnedBusinesses) do
      local _business = GetBusiness(k)
    end

    if not found then
      RemoveBlip(Blips[enteringName])

      Blips[enteringName] = AddBlipForCoord(entering.x,  entering.y,  entering.z)

      SetBlipSprite(Blips[enteringName], 375)
      SetBlipAsShortRange(Blips[enteringName], true)
      SetBlipCategory(Blips[enteringName], 10)

      BeginTextCommandSetBlipName("STRING")
      AddTextComponentString(_U('free_prop'))
      EndTextCommandSetBlipName(Blips[enteringName])
    end
  end
end


function BusinessIsOwned(business)
  return OwnedBusinesses[business.name] == true
end


function OpenBusinessMenu(business)
  ESX.TriggerServerCallback('esx_business:getBusinessIsOwned', function(isOwned)
    --print(isOwned)
    if isOwned == 0 then -- checks if business is owned by anyone
      --print("OpenBusinessMenuDefault")
      OpenBusinessMenuDefault(business)
    else
      --print("OpenBusinessMenuOwned")
      OpenBusinessMenuOwned(business)
    end
  end, business.name)
end


function OpenBusinessMenuDefault(business)
  --print("DEFAULT MENU")
  local elements = {}

  if not Config.EnablePlayerManagement then
    table.insert(elements, {label = _U('buy'), value = 'buy'})
  end

  ESX.UI.Menu.Open(
  'default', GetCurrentResourceName(), 'business',
  {
    title = business.name,
    align    = 'bottom-right',
    elements = elements,
  },
  function(data2, menu)
    menu.close()

    if data2.current.value == 'buy' then
      OpenBusinessTypeMenu(business, owner)
    end
  end,
  function(data, menu)
    menu.close()

    CurrentAction     = 'business_menu'
    CurrentActionMsg  = _U('press_to_menu')
    CurrentActionData = {business = business}
  end
)
end


function OpenBusinessMenuOwned(business)
  --print("OWNED MENU")
  local elements = {}

  if BusinessIsOwned(business) then --checks if business is owned by the client
    table.insert(elements, {label = _U('enter'), value = 'enter'})

    if not Config.EnablePlayerManagement then
      table.insert(elements, {label = _U('sell'), value = 'sell'})
    end
  end

  ESX.TriggerServerCallback('esx_business:getBusinessLabel', function(label)
    ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'business',
    {
      title = label,
      align    = 'bottom-right',
      elements = elements,
    },
    function(data2, menu)
      menu.close()

      if data2.current.value == 'enter' then
        TriggerEvent('instance:create', 'business', {business = business.name, owner = ESX.GetPlayerData().identifier})
      end

      if data2.current.value == 'sell' then
        ESX.TriggerServerCallback('esx_business:getBusinessType', function(type)
          local businessConfig = Config.BusinessSetup[type]

          TriggerServerEvent('esx_business:removeOwnedBusiness', business.name, businessConfig.price) -- how does business.name not return nil here?
        end, business.name)
      end
    end,
    function(data, menu)
      menu.close()

      CurrentAction     = 'business_menu'
      CurrentActionMsg  = _U('press_to_menu')
      CurrentActionData = {business = business}
    end
  )
end, business.name)
end


function OpenBusinessTypeMenu(business, owner)
  local elements = {}

  table.insert(elements, {label = _U('weed_farm'), value = 'weed_farm'})
  table.insert(elements, {label = _U('cocaine_lockup'), value = 'cocaine_lockup'})
  table.insert(elements, {label = _U('meth_lab'), value = 'meth_lab'})
  --table.insert(elements, {label = _U('doc_forge'), value = 'doc_forge'})
  table.insert(elements, {label = _U('money_wash'), value = 'money_wash'})

  ESX.UI.Menu.Open(
  'default', GetCurrentResourceName(), 'choose_type',
  {
    title = "Business",
    align    = 'bottom-right',
    elements = elements,
  },
  function(data2, menu)
    menu.close()

    if data2.current.value == 'weed_farm' then
      ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'enter_name', {
        title = _U('choose_name'),
      }, function(data2, menu)
        menu.close()
        if data2.value ~= "" then
          TriggerServerEvent('esx_business:buyBusiness', business.name, data2.value, "Weed_Farm", Config.BusinessSetup["Weed_Farm"].price)
        end
      end, function(data2,menu)
        menu.close()
      end)
    end

    if data2.current.value == 'cocaine_lockup' then
      ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'enter_name', {
        title = _U('choose_name'),
      }, function(data2, menu)
        menu.close()

        if data2.value ~= "" then
          TriggerServerEvent('esx_business:buyBusiness', business.name, data2.value, "Cocaine_Lockup", Config.BusinessSetup["Cocaine_Lockup"].price)
          --PlaySoundFrontend(-1, "PROPERTY_PURCHASE", "HUD_AWARDS", 1)
        end
      end, function(data2,menu)
        menu.close()
      end)
    end

    if data2.current.value == 'meth_lab' then
      ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'enter_name', {
        title = _U('choose_name'),
      }, function(data2, menu)
        menu.close()

        if data2.value ~= "" then
          TriggerServerEvent('esx_business:buyBusiness', business.name, data2.value, "Meth_Lab", Config.BusinessSetup["Meth_Lab"].price)
          PlaySoundFrontend(-1, "PROPERTY_PURCHASE", "HUD_AWARDS", 1)
        end
      end, function(data2,menu)
        menu.close()
      end)
    end

    if data2.current.value == 'doc_forge' then
      ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'enter_name', {
        title = _U('choose_name'),
      }, function(data2, menu)
        menu.close()

        if data2.value ~= "" then
          TriggerServerEvent('esx_business:buyBusiness', business.name, data2.value, "Doc_Forge", Config.BusinessSetup["Doc_Forge"].price)
          PlaySoundFrontend(-1, "PROPERTY_PURCHASE", "HUD_AWARDS", 1)
        end
      end, function(data2,menu)
        menu.close()
      end)
    end

    if data2.current.value == 'money_wash' then
      ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'enter_name', {
        title = _U('choose_name'),
      }, function(data2, menu)
        menu.close()

        if data2.value ~= "" then
          TriggerServerEvent('esx_business:buyBusiness', business.name, data2.value, "Money_Wash", Config.BusinessSetup["Money_Wash"].price)
          PlaySoundFrontend(-1, "PROPERTY_PURCHASE", "HUD_AWARDS", 1)
        end
      end, function(data2,menu)
        menu.close()
      end)
    end
  end,
  function(data, menu)
    menu.close()

    CurrentAction     = 'business_menu'
    CurrentActionMsg  = _U('press_to_menu')
    CurrentActionData = {business = business}
  end
)
end


function OpenRoomMenu(business, owner)
  local entering = business.entering
  local elements = {}

  table.insert(elements, {label = _U('invite_player'),  value = 'invite_player'})

  if CurrentBusinessOwner == owner then
    --table.insert(elements, {label = _U('player_clothes'), value = 'player_dressing'})
    --table.insert(elements, {label = _U('remove_cloth'), value = 'remove_cloth'})
    table.insert(elements, {label = _U('install_equip'), value = 'install_equip'})
  end

  table.insert(elements, {label = _U('remove_object'),  value = 'room_inventory'})
  table.insert(elements, {label = _U('deposit_object'), value = 'player_inventory'})

  ESX.UI.Menu.CloseAll()

  ESX.TriggerServerCallback('esx_business:getBusinessLabel', function(label)
    ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'room',
    {
      title    = label,
      align    = 'bottom-right',
      elements = elements
    }, function(data, menu)
      if data.current.value == 'invite_player' then
        local playersInArea = ESX.Game.GetPlayersInArea(entering, 10.0)
        local elements      = {}

        for i=1, #playersInArea, 1 do
          if playersInArea[i] ~= PlayerId() then
            table.insert(elements, {label = GetPlayerName(playersInArea[i]), value = playersInArea[i]})
          end
        end

        ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'room_invite',
        {
          title    = _U('invite'),
          align    = 'bottom-right',
          elements = elements,
        }, function(data2, menu2)
          TriggerEvent('instance:invite', 'business', GetPlayerServerId(data2.current.value), {business = business.name, owner = owner})
          ESX.ShowNotification(_U('you_invited', GetPlayerName(data2.current.value)))
        end, function(data2, menu2)
          menu2.close()
        end)
      elseif data.current.value == 'room_inventory' then
        --print("\nsending business: " .. business.name .. " to OpenRoomInventoryMenu")
        OpenRoomInventoryMenu(business, owner) --this isnt getting the right business
      elseif data.current.value == 'player_inventory' then
        --print("\nsending business: " .. business.name .. " to OpenPlayerInventoryMenu")
        OpenPlayerInventoryMenu(business, owner) --this isnt getting the right business
      elseif data.current.value == 'install_equip' then
        --print("\nsending business: " .. business.name .. " to InstallEquipmentMenu")
        InstallEquipmentMenu(business, owner) --this isnt getting the right business
      end
    end, function(data, menu)
      menu.close()

      CurrentAction     = 'room_menu'
      CurrentActionMsg  = _U('press_to_menu')
      CurrentActionData = {business = business, owner = owner}
    end)
  end, business.name)
end

function OpenRoomInventoryMenu(business, owner)

	ESX.TriggerServerCallback('esx_business:getBusinessInventory', function(inventory)

		local elements = {}

		if inventory.blackMoney > 0 then
			table.insert(elements, {label = _U('dirty_money', inventory.blackMoney), type = 'item_account', value = 'black_money'})
		end

		for i=1, #inventory.items, 1 do
			local item = inventory.items[i]
      if item ~= nil and item.label ~= nil and item.count > 0 then -- FIX THIS! SOME ITEMS HAVE NIL LABELS UNTIL ESX_ADDONINVENTORY IS RESET
				table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
      end
		end

		for i=1, #inventory.weapons, 1 do
			local weapon = inventory.weapons[i]
			table.insert(elements, {label = ESX.GetWeaponLabel(weapon.name) .. ' [' .. weapon.ammo .. ']', type = 'item_weapon', value = weapon.name, ammo = weapon.ammo})
		end

    ESX.TriggerServerCallback('esx_business:getBusinessLabel', function(label)
  		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'room_inventory',
  		{
  			title    = label .. ' - ' .. _U('inventory'),
  			align    = 'bottom-right',
  			elements = elements
  		}, function(data, menu)

  			if data.current.type == 'item_weapon' then
  				--menu.close()
          ESX.UI.Menu.CloseAll()

  				TriggerServerEvent('esx_business:getItem', business.name, owner, data.current.type, data.current.value, data.current.ammo)
  				ESX.SetTimeout(300, function()
  					OpenRoomInventoryMenu(business, owner)
  				end)

  			else

  				ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'get_item_count', {
  					title = _U('amount'),
  				}, function(data2, menu)

  					local quantity = tonumber(data2.value)
  					if quantity == nil then
  						ESX.ShowNotification(_U('amount_invalid'))
  					else
  						--menu.close()
              ESX.UI.Menu.CloseAll()

  						TriggerServerEvent('esx_business:getItem', business.name, owner, data.current.type, data.current.value, quantity)
  						ESX.SetTimeout(300, function()
  							OpenRoomInventoryMenu(business, owner)
  						end)
  					end

  				end, function(data2,menu)
  					menu.close()
  				end)

  			end

  		end, function(data, menu)
  			menu.close()
  		end)
    end, business.name)
	end, owner)

end



function OpenPlayerInventoryMenu(business, owner)
  ESX.TriggerServerCallback('esx_business:getPlayerInventory', function(inventory)
    local elements = {}

    if inventory.blackMoney > 0 then
      table.insert(elements, {label = _U('dirty_money', inventory.blackMoney), type = 'item_account', value = 'black_money'})
    end

    for i=1, #inventory.items, 1 do
      local item = inventory.items[i]
      if item.count > 0 then
        table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
      end
    end

    local playerPed  = PlayerPedId()
    local weaponList = ESX.GetWeaponList()

    for i=1, #weaponList, 1 do
      local weaponHash = GetHashKey(weaponList[i].name)
      if HasPedGotWeapon(playerPed, weaponHash, false) and weaponList[i].name ~= 'WEAPON_UNARMED' then
        local ammo = GetAmmoInPedWeapon(playerPed, weaponHash)
        table.insert(elements, {label = weaponList[i].label .. ' [' .. ammo .. ']', type = 'item_weapon', value = weaponList[i].name, ammo = ammo})
      end
    end

    ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'player_inventory',
    {
      title    = _U('inventory'),
      align    = 'bottom-right',
      elements = elements
    }, function(data, menu)

      if data.current.type == 'item_weapon' then
        --menu.close()
        ESX.UI.Menu.CloseAll()

        TriggerServerEvent('esx_business:putItem', business.name, owner, data.current.type, data.current.value, data.current.ammo)
        print(business.name)
        ESX.SetTimeout(300, function()
          OpenPlayerInventoryMenu(business, owner)
        end)
      else

        ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'put_item_count', {
          title = _U('amount'),
        }, function(data2, menu2)
          local quantity = tonumber(data2.value)

          if quantity == nil then
            ESX.ShowNotification(_U('amount_invalid'))
          else
            --menu2.close()
            ESX.UI.Menu.CloseAll()

            TriggerServerEvent('esx_business:putItem', business.name, owner, data.current.type, data.current.value, tonumber(data2.value))
            ESX.SetTimeout(300, function()
              OpenPlayerInventoryMenu(business, owner)
            end)
          end
        end, function(data2, menu2)
          menu2.close()
        end)
      end
    end, function(data, menu)
      menu.close()
    end)
  end)
end


function InstallEquipmentMenu(business, owner)
  ESX.TriggerServerCallback('esx_business:getPlayerInventory', function(inventory)
    local elements = {}

    local interiorID = GetInteriorFromEntity(PlayerPedId())

    for i=1, #inventory.items, 1 do
      local item = inventory.items[i]

      if Config.BusinessSetup.Weed_Farm.interiorID == interiorID then
        --local businessConfig = Config.BusinessSetup.Weed_Farm

        if item.name == "weed_equip_basic" and not IsInteriorPropEnabled(interiorID, "weed_set_up") then
          if not IsInteriorPropEnabled(interiorID, "weed_upgrade_equip") then
            if item.count > 0 then
              table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
            end
          end
        elseif item.name == "weed_equip_upgrade" and not IsInteriorPropEnabled(interiorID, "weed_upgrade_equip") then
          if item.count > 0 then
            table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
          end
        end
      end

      if Config.BusinessSetup.Cocaine_Lockup.interiorID == interiorID then
        --local businessConfig = Config.BusinessSetup.Cocaine_Lockup

        if item.name == "coke_equip_basic" and not IsInteriorPropEnabled(interiorID, "table_equipment") then
          if not IsInteriorPropEnabled(interiorID, "table_equipment_upgrade") then
            if item.count > 0 then
              table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
            end
          end
        elseif item.name == "coke_equip_upgrade" and not IsInteriorPropEnabled(interiorID, "table_equipment_upgrade") then
          if item.count > 0 then
            table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
          end
        end
      end

      if Config.BusinessSetup.Meth_Lab.interiorID == interiorID then
        --local businessConfig = Config.BusinessSetup.Meth_Lab

        if item.name == "meth_equip_basic" and not IsInteriorPropEnabled(interiorID, "meth_lab_basic") then
          if not IsInteriorPropEnabled(interiorID, "meth_lab_upgrade") then
            if item.count > 0 then
              table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
            end
          end
        elseif item.name == "meth_equip_upgrade" and not IsInteriorPropEnabled(interiorID, "meth_lab_upgrade") then
          if item.count > 0 then
            table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
          end
        end
      end

      if Config.BusinessSetup.Doc_Forge.interiorID == interiorID then
        --local businessConfig = Config.BusinessSetup.Doc_Forge

        if item.name == "doc_forge_equip_basic" and not IsInteriorPropEnabled(interiorID, "equipment_basic") then
          if not IsInteriorPropEnabled(interiorID, "equipment_upgrade") then
            if item.count > 0 then
              table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
            end
          end
        elseif item.name == "doc_forge_equip_upgrade" and not IsInteriorPropEnabled(interiorID, "equipment_upgrade") then
          if item.count > 0 then
            table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
          end
        elseif item.name == "doc_forge_interior_upgrade" and not IsInteriorPropEnabled(interiorID, "interior_upgrade") then
          if item.count > 0 then
            table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
          end
        end
      end

      if Config.BusinessSetup.Money_Wash.interiorID == interiorID then
        --local businessConfig = Config.BusinessSetup.Money_Wash

        if item.name == "money_wash_equip_basic" and not IsInteriorPropEnabled(interiorID, "counterfeit_standard_equip_no_prod") then
          if not IsInteriorPropEnabled(interiorID, "counterfeit_upgrade_equip_no_prod") then
            if item.count > 0 then
              table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
            end
          end
        elseif item.name == "money_wash_equip_upgrade" and not IsInteriorPropEnabled(interiorID, "counterfeit_upgrade_equip_no_prod") then
          if item.count > 0 then
            table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
          end
        elseif item.name == "money_wash_dryera" and not IsInteriorPropEnabled(interiorID, "dryera_on") and not IsInteriorPropEnabled(interiorID, "dryera_off") and not IsInteriorPropEnabled(interiorID, "dryera_open")then
          if item.count > 0 then
            table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
          end
        elseif item.name == "money_wash_dryerb" and not IsInteriorPropEnabled(interiorID, "dryerb_on") and not IsInteriorPropEnabled(interiorID, "dryerb_off") and not IsInteriorPropEnabled(interiorID, "dryerb_open") then
          if item.count > 0 then
            table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
          end
        elseif item.name == "money_wash_dryerc" and not IsInteriorPropEnabled(interiorID, "dryerc_on") and not IsInteriorPropEnabled(interiorID, "dryerc_off") and not IsInteriorPropEnabled(interiorID, "dryerc_open") then
          if item.count > 0 then
            table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
          end
        elseif item.name == "money_wash_dryerd" and not IsInteriorPropEnabled(interiorID, "dryerd_on") and not IsInteriorPropEnabled(interiorID, "dryerd_off") and not IsInteriorPropEnabled(interiorID, "dryerd_open") then
          if item.count > 0 then
            table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
          end
        end
      end
    end

    ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'install_equip',
    {
      title    = _U('equipment'),
      align    = 'bottom-right',
      elements = elements,
    },
    function(data, menu)
      local item = data.current.value

      menu.close()

      if Config.BusinessSetup.Weed_Farm.interiorID == interiorID then
        TriggerServerEvent('esx_business:installEquipment', item, hostThisInstance)

        if item == "weed_equip_basic" then
          TriggerServerEvent('esx_business:enableProp', "weed_set_up", hostThisInstance, business)
        elseif item == "weed_equip_upgrade" then
          TriggerServerEvent('esx_business:disableProp', "weed_set_up", hostThisInstance, business)
          Wait(1000)
          TriggerServerEvent('esx_business:enableProp', "weed_upgrade_equip", hostThisInstance, business)
        end
      end

      if Config.BusinessSetup.Cocaine_Lockup.interiorID == interiorID then
        TriggerServerEvent('esx_business:installEquipment', item, hostThisInstance)

        local equip_basic = {'coke_press_basic', 'production_basic', 'table_equipment'}
        local equip_upgrade = {'coke_press_upgrade', 'production_upgrade', 'table_equipment_upgrade', "equipment_upgrade"}

        if item == "coke_equip_basic" then
          for _,prop in ipairs(equip_basic) do
            TriggerServerEvent('esx_business:enableProp', prop, hostThisInstance, business)
            Wait(1000)
          end
        elseif item == "coke_equip_upgrade" then
          for _,prop in ipairs(equip_basic) do
            TriggerServerEvent('esx_business:disableProp', prop, hostThisInstance, business)
            Wait(1000)
          end
          for _,prop in ipairs(equip_upgrade) do
            TriggerServerEvent('esx_business:enableProp', prop, hostThisInstance, business)
            Wait(1000)
          end
        end
      end

      if Config.BusinessSetup.Meth_Lab.interiorID == interiorID then
        TriggerServerEvent('esx_business:installEquipment', item, hostThisInstance)

        if item == "meth_equip_basic" then
          TriggerServerEvent('esx_business:enableProp', "meth_lab_basic", hostThisInstance, business)
        elseif item == "meth_equip_upgrade" then
          TriggerServerEvent('esx_business:disableProp', "meth_lab_basic", hostThisInstance, business)
          Wait(1000)
          TriggerServerEvent('esx_business:enableProp', "meth_lab_upgrade", hostThisInstance, business)
        end
      end

      if Config.BusinessSetup.Doc_Forge.interiorID == interiorID then
        TriggerServerEvent('esx_business:installEquipment', item, hostThisInstance)

        if item == "doc_forge_equip_basic" then
          TriggerServerEvent('esx_business:enableProp', "equipment_basic", hostThisInstance, business)
        elseif item == "doc_forge_equip_upgrade" then
          TriggerServerEvent('esx_business:disableProp', "equipment_basic", hostThisInstance, business)
          Wait(1000)
          TriggerServerEvent('esx_business:enableProp', "equipment_upgrade", hostThisInstance, business)
        elseif item == "doc_forge_interior_upgrade" then
          TriggerServerEvent('esx_business:disableProp', "interior_basic", hostThisInstance, business)
          Wait(1000)
          TriggerServerEvent('esx_business:enableProp', "interior_upgrade", hostThisInstance, business)
        end
      end

      if Config.BusinessSetup.Money_Wash.interiorID == interiorID then
        TriggerServerEvent('esx_business:installEquipment', item, hostThisInstance)

        if item == "money_wash_equip_basic" then
          TriggerServerEvent('esx_business:enableProp', 'counterfeit_standard_equip_no_prod', hostThisInstance, business)
        elseif item == "money_wash_equip_upgrade" then
          TriggerServerEvent('esx_business:disableProp', 'counterfeit_standard_equip_no_prod', hostThisInstance, business)
          Wait(1000)
          TriggerServerEvent('esx_business:enableProp', 'counterfeit_upgrade_equip_no_prod', hostThisInstance, business)
        elseif item == "money_wash_dryera" then
          TriggerServerEvent('esx_business:enableProp', "dryera_off", hostThisInstance, business)
        elseif item == "money_wash_dryerb" then
          TriggerServerEvent('esx_business:enableProp', "dryerb_off", hostThisInstance, business)
        elseif item == "money_wash_dryerc" then
          TriggerServerEvent('esx_business:enableProp', "dryerc_off", hostThisInstance, business)
        elseif item == "money_wash_dryerd" then
          TriggerServerEvent('esx_business:enableProp', "dryerd_off", hostThisInstance, business)
        end
      end

      menu.close()
    end,
    function(data, menu)
      menu.close()
    end)
  end)
end

--[[function getBusinessConfig()
local interiorID = GetInteriorFromEntity(PlayerPedId())

if Config.BusinessSetup.Weed_Farm.interiorID == interiorID then
return Config.BusinessSetup.Weed_Farm
elseif Config.BusinessSetup.Cocaine_Lockup.interiorID == interiorID then
return Config.BusinessSetup.Cocaine_Lockup
elseif Config.BusinessSetup.Meth_Lab.interiorID == interiorID then
return Config.BusinessSetup.Meth_Lab
elseif Config.BusinessSetup.Doc_Forge.interiorID == interiorID then
return Config.BusinessSetup.Doc_Forge
elseif Config.BusinessSetup.Money_Wash.interiorID == interiorID then
return Config.BusinessSetup.Money_Wash
end
end]]



AddEventHandler('instance:loaded', function()
  TriggerEvent('instance:registerType', 'business',
  function(instance)
    EnterBusiness(instance.data.business, instance.data.owner)
  end,
  function(instance)
    ExitBusiness(instance.data.business)
  end
)

end)


AddEventHandler('playerSpawned', function()
  if FirstSpawn then
    Citizen.CreateThread(function()
      while not ESX.IsPlayerLoaded() do
        Citizen.Wait(0)
      end

      ESX.TriggerServerCallback('esx_business:getLastBusiness', function(businessName)
        if businessName ~= nil then
          TriggerEvent('instance:create', 'business', {business = businessName, owner = ESX.GetPlayerData().identifier})
        end
      end)
    end)
    FirstSpawn = false
  end
end)


function resetWeedFarm()
  local BikerWeedFarm = exports.bob74_ipl:GetBikerWeedFarmObject()
  BikerWeedFarm.Style.Clear()
  BikerWeedFarm.Security.Clear()
  BikerWeedFarm.Plant1.Clear()
  BikerWeedFarm.Plant2.Clear()
  BikerWeedFarm.Plant3.Clear()
  BikerWeedFarm.Plant4.Clear()
  BikerWeedFarm.Plant5.Clear()
  BikerWeedFarm.Plant6.Clear()
  BikerWeedFarm.Plant7.Clear()
  BikerWeedFarm.Plant8.Clear()
  BikerWeedFarm.Plant9.Clear()

  BikerWeedFarm.Style.Set(BikerWeedFarm.Style.basic)
  BikerWeedFarm.Details.Enable(BikerWeedFarm.Details.drying, false)
  BikerWeedFarm.Details.Enable(BikerWeedFarm.Details.chairs, true)
  BikerWeedFarm.Details.Enable(BikerWeedFarm.Details.production, false)

  RefreshInterior(BikerWeedFarm.interiorId)
end

function resetCocaineLockup()
  local BikerCocaine = exports.bob74_ipl:GetBikerCocaineObject()
  BikerCocaine.Style.Clear()
  BikerCocaine.Security.Clear()
  BikerCocaine.Style.Set(BikerCocaine.Style.basic)

  BikerCocaine.Details.Enable(BikerCocaine.Details.cokeBasic1, false)
  BikerCocaine.Details.Enable(BikerCocaine.Details.cokeBasic2, false)
  BikerCocaine.Details.Enable(BikerCocaine.Details.cokeBasic3, false)
  BikerCocaine.Details.Enable(BikerCocaine.Details.cokeBasic4, false)
  BikerCocaine.Details.Enable(BikerCocaine.Details.cokeBasic5, false)

  RefreshInterior(BikerCocaine.interiorId)
end

function resetMethLab()
  local BikerMethLab = exports.bob74_ipl:GetBikerMethLabObject()
  BikerMethLab.Style.Clear()
  BikerMethLab.Style.Set(BikerMethLab.Style.empty)
  BikerMethLab.Security.Clear()
  BikerMethLab.Details.Enable(BikerMethLab.Details.production, false)

  RefreshInterior(BikerMethLab.interiorId)
end

function resetMoneyWash()
  local BikerCounterfeit = exports.bob74_ipl:GetBikerCounterfeitObject()
  BikerCounterfeit.Security.Clear()
  BikerCounterfeit.Printer.Set(BikerCounterfeit.Printer.none)
  BikerCounterfeit.Dryer1.Clear()
  BikerCounterfeit.Dryer2.Clear()
  BikerCounterfeit.Dryer3.Clear()
  BikerCounterfeit.Dryer4.Clear()

  BikerCounterfeit.Details.Enable(BikerCounterfeit.Details.Cash100, false)
  BikerCounterfeit.Details.Enable(BikerCounterfeit.Details.chairs, false)
  BikerCounterfeit.Details.Enable(BikerCounterfeit.Details.cutter, false)
  BikerCounterfeit.Details.Enable(BikerCounterfeit.Details.furnitures, false)

  RefreshInterior(BikerCounterfeit.interiorId)
end

function resetDocForge()
  local BikerDocumentForgery = exports.bob74_ipl:GetBikerDocumentForgeryObject()
  BikerDocumentForgery.Style.Clear()
  BikerDocumentForgery.Style.Set(BikerDocumentForgery.Style.basic)
  BikerDocumentForgery.Security.Clear()
  BikerDocumentForgery.Equipment.Clear()

  BikerDocumentForgery.Details.Enable(BikerDocumentForgery.Details.production, false)
  BikerDocumentForgery.Details.Enable(BikerDocumentForgery.Details.setup, false)
  BikerDocumentForgery.Details.Enable(BikerDocumentForgery.Details.clutter, false)
  BikerDocumentForgery.Details.Enable(BikerDocumentForgery.Details.Chairs, false)

  RefreshInterior(BikerDocumentForgery.interiorId)
end


AddEventHandler('esx_business:resetProps', function(business)
  Citizen.Trace("\nresetProps")

  while GetInteriorFromEntity(PlayerPedId()) == 0 do --if the player enters from outside, wait till shit loads
    Citizen.Wait(0)
  end
  local interiorID = GetInteriorFromEntity(PlayerPedId())

  local coords = GetEntityCoords(PlayerPedId())
  local interiorID = GetInteriorAtCoords(coords)

  if Config.BusinessSetup.Weed_Farm.interiorID == interiorID then
    resetWeedFarm()
  elseif Config.BusinessSetup.Cocaine_Lockup.interiorID == interiorID then
    resetCocaineLockup()
  elseif Config.BusinessSetup.Meth_Lab.interiorID == interiorID then
    resetMethLab()
  elseif Config.BusinessSetup.Money_Wash.interiorID == interiorID then
    resetMoneyWash()
  elseif Config.BusinessSetup.Doc_Forge.interiorID == interiorID then
    resetDocForge()
  end

  for k,businessConfig in pairs(Config.BusinessSetup) do
    if businessConfig.interiorID == interiorID then --interiorID is 0 here
      for _,prop in ipairs(businessConfig.props) do
        if IsInteriorPropEnabled(interiorID, prop) then
          --print("\nDISABLE INTERIOR PROP FOR RESET: " .. prop)
          TriggerEvent('esx_business:disableProp', prop, hostThisInstance) -- client only
        end
      end
    end
  end

  ESX.TriggerServerCallback('esx_business:getProps', function(props, hostThisInstance)
    --print("\nGot props for onEnter: " .. json.encode(props))
    for _,prop in ipairs(props) do
      TriggerEvent('esx_business:enableProp', prop, hostThisInstance) --this only needs to be local
    end
  end, business.name, hostThisInstance)
end)


AddEventHandler('esx_business:getBusinesses', function(cb)
  cb(GetBusinesses())
end)


AddEventHandler('esx_business:getBusiness', function(name, cb)
  cb(GetBusiness(name))
end)


RegisterNetEvent('esx_business:setBusinessOwned') -- only gets called on client who buys business
AddEventHandler('esx_business:setBusinessOwned', function(name, owned)
  SetBusinessOwned(name, owned)
end)


RegisterNetEvent('esx_business:enableProp')
AddEventHandler('esx_business:enableProp', function(prop, host)
  Citizen.Trace("\nenableProp: " .. prop)

  while GetInteriorFromEntity(PlayerPedId()) == 0 do --if the player enters from outside, wait till shit loads
    Citizen.Wait(0)
  end
  local interiorID = GetInteriorFromEntity(PlayerPedId())

  Citizen.Trace("\ninteriorID: " .. interiorID)
  if interiorID ~= 0 then
    if hostThisInstance == host then  --hostThisInstance defined in instance onEnter
      if not IsInteriorPropEnabled(interiorID, prop) then
        EnableInteriorProp(interiorID, prop)
        RefreshInterior(interiorID)

        Citizen.Trace("\nEnabled Prop: " .. prop)
      else
        Citizen.Trace("\nProp already enabled: " .. prop)
      end
    end
  end
end)


RegisterNetEvent('esx_business:disableProp')
AddEventHandler('esx_business:disableProp', function(prop, host)
  Citizen.Trace("\ndisableProp: " .. prop)

  local interiorID = GetInteriorFromEntity(PlayerPedId())
  while interiorID == 0 do --if the player enters from outside, wait till shit loads
    Citizen.Wait(0)
  end

  Citizen.Trace("\ninteriorID: " .. interiorID)

  if hostThisInstance == host then   --hostThisInstance defined in instance onEnter
    if IsInteriorPropEnabled(interiorID, prop) then
      DisableInteriorProp(interiorID, prop)
      RefreshInterior(interiorID)

      Citizen.Trace("\nDisabled Prop: " .. prop)
    else
      Citizen.Trace("\nProp already disabled: " .. prop)
    end
  end
end)


RegisterNetEvent('instance:onCreate')
AddEventHandler('instance:onCreate', function(instance)
  if instance.type == 'business' then
    TriggerEvent('instance:enter', instance)
  end
end)


RegisterNetEvent('instance:onEnter')
AddEventHandler('instance:onEnter', function(instance)
  print("on enter")
  while GetInteriorFromEntity(PlayerPedId()) == 0 do  --if the player enters from outside, wait till shit loads
    Citizen.Wait(0)
  end

  if instance.type == 'business' then
    local business = GetBusiness(instance.data.business) -- make this global so our updateprops function can access it
    local isHost   = GetPlayerFromServerId(instance.host) == PlayerId()
    hostThisInstance = GetPlayerFromServerId(instance.host)

    local isOwned  = false

    if BusinessIsOwned(business) == true then
      isOwned = true
    end

    if(isOwned or not isHost) then
      HasChest = true
    else
      HasChest = false
    end

    --[[local roomMenu = getBusinessConfig().zones.roomMenu
    roomMenuBlip = AddBlipForCoord(roomMenu.x,  roomMenu.y,  roomMenu.z)
    SetBlipSprite(roomMenuBlip, 606)
    SetBlipAsShortRange(roomMenuBlip, true)
    BeginTextCommandSetBlipName("STRING")
    AddTextComponentString(_U('room_menu'))
    EndTextCommandSetBlipName(roomMenuBlip)]]

    TriggerEvent('esx_business:resetProps', business)
  end
end)


RegisterNetEvent('instance:onPlayerLeft')
AddEventHandler('instance:onPlayerLeft', function(instance, player)
  if player == instance.host then
    TriggerEvent('instance:leave')
  end
  RemoveBlip(roomMenuBlip)
end)



AddEventHandler('esx_business:hasEnteredMarker', function(name, part)
  local business = GetBusiness(name)
  --print("\nbusiness in hasEnteredMarker: " .. name)

  if part == 'chemicalPlant_Heist' then
    CurrentAction     = 'chemical_plant_heist'
    CurrentActionMsg  = _U('press_to_extract')
  end

  if part == 'chemicalPlant_Reset' then
    CurrentAction     = 'chemical_plant_reset'
    CurrentActionMsg  = _U('press_to_reset_alarm')
  end

  if part == 'entering' then
    CurrentAction     = 'business_menu'
    CurrentActionMsg  = _U('press_to_menu')
    CurrentActionData = {business = business}
  end

  if part == 'exit' then
    CurrentAction     = 'room_exit'
    CurrentActionMsg  = _U('press_to_exit')
    CurrentActionData = {businessName = name}
  end

  if part == 'roomMenu' then
    CurrentAction     = 'room_menu'
    CurrentActionMsg  = _U('press_to_menu')
    CurrentActionData = {business = business, owner = CurrentBusinessOwner}
  end


  local interiorID = GetInteriorFromEntity(PlayerPedId())

  if Config.BusinessSetup.Weed_Farm.interiorID == interiorID then
    local businessConfig = Config.BusinessSetup.Weed_Farm

    if part == 'weedDrying' then
      if not IsInteriorPropEnabled(businessConfig.interiorID, 'weed_drying') then
        CurrentAction     = 'weed_drying'
        CurrentActionMsg  = _U('press_to_dry','Undried Marijuana')
        CurrentActionData = {business = business, owner = CurrentBusinessOwner}
      end
    end

    if part == 'weedProcess' then
      CurrentAction     = 'weed_process'
      CurrentActionMsg  = _U('press_to_process','Dried Marijuana','Bag of Marijuana')
      CurrentActionData = {business = business, owner = CurrentBusinessOwner}
    end

    for i, name in ipairs(businessConfig.planterVars) do
      if part == name then
        if getGrowStage(businessConfig.planterLetters[i]) == 0 then
          CurrentAction     = businessConfig.planterSetupVars[i]
          CurrentActionMsg  = _U('press_to_setup','Marijuana Seed')
        elseif getGrowStage(businessConfig.planterLetters[i]) == 3 then
          CurrentAction     = businessConfig.planterHarvestVars[i]
          CurrentActionMsg  = _U('press_to_harvest','Undried Weed')
        end
        CurrentActionData = {business = business, owner = CurrentBusinessOwner}
      end
    end
  end


  if Config.BusinessSetup.Cocaine_Lockup.interiorID == interiorID then
    local businessConfig = Config.BusinessSetup.Cocaine_Lockup

    for i, name in ipairs(businessConfig.cokeVars) do
      if part == name then
        if not isCokeCut(businessConfig.cokeNumbers[i]) then
          CurrentAction     = businessConfig.cokeSetupVars[i]
          CurrentActionMsg  = _U('press_to_process','Unprocessed Cocaine','Bag of Cocaine')
        end
      end
      CurrentActionData = {business = business, owner = CurrentBusinessOwner}
    end
  end


  if Config.BusinessSetup.Meth_Lab.interiorID == interiorID then
    local businessConfig = Config.BusinessSetup.Meth_Lab

    if part == 'methProduction' then
      CurrentAction     = 'meth_production'
      CurrentActionMsg  = _U('press_to_produce','Methamphetamine')
      CurrentActionData = {business = business, owner = CurrentBusinessOwner}
    end

    if part == 'methProcess' then
      CurrentAction     = 'meth_process'
      CurrentActionMsg  = _U('press_to_process','Unprocessed Methamphetamine','Bag of Methamphetamine')
      CurrentActionData = {business = business, owner = CurrentBusinessOwner}
    end
  end


  if Config.BusinessSetup.Doc_Forge.interiorID == interiorID then
    local businessConfig = Config.BusinessSetup.Doc_Forge

    if part == 'production' then
      CurrentAction     = 'doc_production'
      CurrentActionMsg  = _U('press_to_produce','Counterfeit Documents')
      CurrentActionData = {business = business, owner = CurrentBusinessOwner}
    end
  end


  if Config.BusinessSetup.Money_Wash.interiorID == interiorID then
    local businessConfig = Config.BusinessSetup.Money_Wash

    for i, name in ipairs(businessConfig.dryerVars) do
      if part == name then
        if getDryerState(business, businessConfig.dryerLetters[i]) == "Off" then
          CurrentAction     = businessConfig.dryerSetupVars[i]
          CurrentActionMsg  = _U('press_to_wash','Unwashed Money')
        elseif getDryerState(business, businessConfig.dryerLetters[i]) == "Open" then
          CurrentAction     = businessConfig.dryerTakeVars[i]
          CurrentActionMsg  = _U('press_to_take','Washed Money')
        end
        CurrentActionData = {business = business, owner = CurrentBusinessOwner}
      end
    end

    if part == 'printerStart' then
      CurrentAction     = 'printer_start'
      CurrentActionMsg  = _U('press_to_print','Unprocessed Counterfeit Notes')
      CurrentActionData = {business = business, owner = CurrentBusinessOwner}
    end

    if part == 'printerTake' then
      CurrentAction     = 'printer_take'
      CurrentActionMsg  = _U('press_to_take','Unprocessed Counterfeit Notes')
      CurrentActionData = {business = business, owner = CurrentBusinessOwner}
    end

    if part == 'production' then
      CurrentAction     = 'money_production'
      CurrentActionMsg  = _U('press_to_produce','Counterfeit Notes')
      CurrentActionData = {business = business, owner = CurrentBusinessOwner}
    end
  end
end)


AddEventHandler('esx_business:hasExitedMarker', function(name, part)
  local business = GetBusiness(name)
  --print("\nbusiness in hasExitedMarker: " .. name)
  ESX.UI.Menu.CloseAll()
  CurrentAction = nil
end)



-- Display markers
Citizen.CreateThread(function()
  while PlayerData == nil do
    Citizen.Wait(0)
  end

  while true do
    Wait(0)

    local coords = GetEntityCoords(GetPlayerPed(-1))
    local interiorID = GetInteriorFromEntity(PlayerPedId())

    --Meth Heist Zones
    for k,pos in pairs(Config.HeistZones.chemicalPlant) do
      if (GetDistanceBetweenCoords(coords, pos.x, pos.y, pos.z, true) < Config.DrawDistance) and PlayerData.job.name ~= 'police' then
        DrawMarker(Config.MarkerType, pos.x, pos.y, pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 100, false, true, 2, false, false, false, false)
      end
    end

    if (GetDistanceBetweenCoords(coords, 2664.12, 1642.01, 23.87, true) < Config.DrawDistance) and PlayerData.job.name == 'police' then
      DrawMarker(Config.MarkerType, 2664.12, 1642.01, 23.87, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, 0, 0, 255, 100, false, true, 2, false, false, false, false)
    end


    for i=1, #Config.Businesses, 1 do
      local business = Config.Businesses[i]

      --entering (the outside portal)
      if (not business.disabled and GetDistanceBetweenCoords(coords, business.entering.x, business.entering.y, business.entering.z, true) < Config.DrawDistance) then
        DrawMarker(Config.MarkerType, business.entering.x, business.entering.y, business.entering.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.EnterExitMarkerColor.r, Config.EnterExitMarkerColor.g, Config.EnterExitMarkerColor.b, 100, false, true, 2, false, false, false, false)
      end


      if Config.BusinessSetup.Weed_Farm.interiorID == interiorID then
        local businessConfig = Config.BusinessSetup.Weed_Farm
        local zones = businessConfig.zones

        --exit (the inside portal)
        if (not business.disabled and GetDistanceBetweenCoords(coords, zones.exit.x, zones.exit.y, zones.exit.z, true) < Config.DrawDistance) then
          DrawMarker(Config.MarkerType, zones.exit.x, zones.exit.y, zones.exit.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.EnterExitMarkerColor.r, Config.EnterExitMarkerColor.g, Config.EnterExitMarkerColor.b, 100, false, true, 2, false, false, false, false)
        end

        --room menu portal
        if (HasChest and not business.disabled and GetDistanceBetweenCoords(coords, zones.roomMenu.x, zones.roomMenu.y, zones.roomMenu.z, true) < Config.DrawDistance) then
          DrawMarker(Config.MarkerType, zones.roomMenu.x, zones.roomMenu.y, zones.roomMenu.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.RoomMenuMarkerColor.r, Config.RoomMenuMarkerColor.g, Config.RoomMenuMarkerColor.b, 100, false, true, 2, false, false, false, false)
        end

        for k,pos in pairs(zones.planters) do
          local i = k
          if (not business.disabled and GetDistanceBetweenCoords(coords, pos.x, pos.y, pos.z, true) < Config.DrawDistance) then
            if IsInteriorPropEnabled(businessConfig.interiorID, 'weed_set_up') or IsInteriorPropEnabled(businessConfig.interiorID, 'weed_upgrade_equip') then
              if getGrowStage(businessConfig.planterLetters[i]) == 0 or getGrowStage(businessConfig.planterLetters[i]) == 3 then
                DrawMarker(Config.MarkerType, pos.x, pos.y, pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 100, false, true, 2, false, false, false, false)
              end
            end
          end
        end

        --drying
        if (not business.disabled and not IsInteriorPropEnabled(businessConfig.interiorID, 'weed_drying') and GetDistanceBetweenCoords(coords, zones.drying.x, zones.drying.y, zones.drying.z, true) < Config.DrawDistance) then
          DrawMarker(Config.MarkerType, zones.drying.x, zones.drying.y, zones.drying.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 100, false, true, 2, false, false, false, false)
        end

        --producion
        for k,pos in pairs(zones.production) do
          if (not business.disabled and not IsInteriorPropEnabled(businessConfig.interiorID, 'weed_production') and GetDistanceBetweenCoords(coords, pos.x, pos.y, pos.z, true) < Config.DrawDistance) then
            DrawMarker(Config.MarkerType, pos.x, pos.y, pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 100, false, true, 2, false, false, false, false)
          end
        end
      end


      if Config.BusinessSetup.Cocaine_Lockup.interiorID == interiorID then
        local businessConfig = Config.BusinessSetup.Cocaine_Lockup
        local zones = businessConfig.zones

        --exit (the inside portal)
        if (not business.disabled and GetDistanceBetweenCoords(coords, zones.exit.x, zones.exit.y, zones.exit.z, true) < Config.DrawDistance) then
          DrawMarker(Config.MarkerType, zones.exit.x, zones.exit.y, zones.exit.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.EnterExitMarkerColor.r, Config.EnterExitMarkerColor.g, Config.EnterExitMarkerColor.b, 100, false, true, 2, false, false, false, false)
        end

        --room menu portal
        if (HasChest and not business.disabled and GetDistanceBetweenCoords(coords, zones.roomMenu.x, zones.roomMenu.y, zones.roomMenu.z, true) < Config.DrawDistance) then
          DrawMarker(Config.MarkerType, zones.roomMenu.x, zones.roomMenu.y, zones.roomMenu.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.RoomMenuMarkerColor.r, Config.RoomMenuMarkerColor.g, Config.RoomMenuMarkerColor.b, 100, false, true, 2, false, false, false, false)
        end

        for k,pos in pairs(zones.production) do
          local i = k
          if (not business.disabled and GetDistanceBetweenCoords(coords, pos.x, pos.y, pos.z, true) < Config.DrawDistance) then
            if IsInteriorPropEnabled(businessConfig.interiorID, 'table_equipment') or IsInteriorPropEnabled(businessConfig.interiorID, 'table_equipment_upgrade') then
              if not isCokeCut(businessConfig.cokeNumbers[i]) then
                DrawMarker(Config.MarkerType, pos.x, pos.y, pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 100, false, true, 2, false, false, false, false)
              end
            end
          end
        end
        for k,pos in pairs(zones.production_upgrade) do
          local i = k + 3 --offset
          if (not business.disabled and GetDistanceBetweenCoords(coords, pos.x, pos.y, pos.z, true) < Config.DrawDistance) then
            if IsInteriorPropEnabled(businessConfig.interiorID, 'table_equipment_upgrade') then
              if not isCokeCut(businessConfig.cokeNumbers[i]) then
                DrawMarker(Config.MarkerType, pos.x, pos.y, pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 100, false, true, 2, false, false, false, false)
              end
            end
          end
        end
      end


      if Config.BusinessSetup.Meth_Lab.interiorID == interiorID then
        local businessConfig = Config.BusinessSetup.Meth_Lab
        local zones = businessConfig.zones

        --exit (the inside portal)
        if (not business.disabled and GetDistanceBetweenCoords(coords, zones.exit.x, zones.exit.y, zones.exit.z, true) < Config.DrawDistance) then
          DrawMarker(Config.MarkerType, zones.exit.x, zones.exit.y, zones.exit.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.EnterExitMarkerColor.r, Config.EnterExitMarkerColor.g, Config.EnterExitMarkerColor.b, 100, false, true, 2, false, false, false, false)
        end

        --room menu portal
        if (HasChest and not business.disabled and GetDistanceBetweenCoords(coords, zones.roomMenu.x, zones.roomMenu.y, zones.roomMenu.z, true) < Config.DrawDistance) then
          DrawMarker(Config.MarkerType, zones.roomMenu.x, zones.roomMenu.y, zones.roomMenu.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.RoomMenuMarkerColor.r, Config.RoomMenuMarkerColor.g, Config.RoomMenuMarkerColor.b, 100, false, true, 2, false, false, false, false)
        end

        --production
        if (not business.disabled and GetDistanceBetweenCoords(coords, zones.production.x, zones.production.y, zones.production.z, true) < Config.DrawDistance) then
          if IsInteriorPropEnabled(businessConfig.interiorID, 'meth_lab_basic') or IsInteriorPropEnabled(businessConfig.interiorID, 'meth_lab_upgrade') then
            DrawMarker(Config.MarkerType, zones.production.x, zones.production.y, zones.production.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 100, false, true, 2, false, false, false, false)
          end
        end

        --processing
        if (not business.disabled and GetDistanceBetweenCoords(coords, zones.processing.x, zones.processing.y, zones.processing.z, true) < Config.DrawDistance) then
          if not IsInteriorPropEnabled(businessConfig.interiorID, 'meth_lab_production') then
            DrawMarker(Config.MarkerType, zones.processing.x, zones.processing.y, zones.processing.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 100, false, true, 2, false, false, false, false)
          end
        end
      end


      if Config.BusinessSetup.Doc_Forge.interiorID == interiorID then
        local businessConfig = Config.BusinessSetup.Doc_Forge
        local zones = businessConfig.zones

        --exit (the inside portal)
        if (not business.disabled and GetDistanceBetweenCoords(coords, zones.exit.x, zones.exit.y, zones.exit.z, true) < Config.DrawDistance) then
          DrawMarker(Config.MarkerType, zones.exit.x, zones.exit.y, zones.exit.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.EnterExitMarkerColor.r, Config.EnterExitMarkerColor.g, Config.EnterExitMarkerColor.b, 100, false, true, 2, false, false, false, false)
        end

        --room menu portal
        if (HasChest and not business.disabled and GetDistanceBetweenCoords(coords, zones.roomMenu.x, zones.roomMenu.y, zones.roomMenu.z, true) < Config.DrawDistance) then
          DrawMarker(Config.MarkerType, zones.roomMenu.x, zones.roomMenu.y, zones.roomMenu.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.RoomMenuMarkerColor.r, Config.RoomMenuMarkerColor.g, Config.RoomMenuMarkerColor.b, 100, false, true, 2, false, false, false, false)
        end

        for k,pos in pairs(zones.production) do
          if (not business.disabled and GetDistanceBetweenCoords(coords, pos.x, pos.y, pos.z, true) < Config.DrawDistance) then
            if not IsInteriorPropEnabled(businessConfig.interiorID, 'production') then
              DrawMarker(Config.MarkerType, pos.x, pos.y, pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 100, false, true, 2, false, false, false, false)
            end
          end
        end
      end


      if Config.BusinessSetup.Money_Wash.interiorID == interiorID then
        local businessConfig = Config.BusinessSetup.Money_Wash
        local zones = businessConfig.zones

        --exit (the inside portal)
        if (not business.disabled and GetDistanceBetweenCoords(coords, zones.exit.x, zones.exit.y, zones.exit.z, true) < Config.DrawDistance) then
          DrawMarker(Config.MarkerType, zones.exit.x, zones.exit.y, zones.exit.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.EnterExitMarkerColor.r, Config.EnterExitMarkerColor.g, Config.EnterExitMarkerColor.b, 100, false, true, 2, false, false, false, false)
        end

        --room menu portal
        if (HasChest and not business.disabled and GetDistanceBetweenCoords(coords, zones.roomMenu.x, zones.roomMenu.y, zones.roomMenu.z, true) < Config.DrawDistance) then
          DrawMarker(Config.MarkerType, zones.roomMenu.x, zones.roomMenu.y, zones.roomMenu.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.RoomMenuMarkerColor.r, Config.RoomMenuMarkerColor.g, Config.RoomMenuMarkerColor.b, 100, false, true, 2, false, false, false, false)
        end

        --printer start (basic)
        if (not business.disabled and GetDistanceBetweenCoords(coords, zones.printerStartBasic.x, zones.printerStartBasic.y, zones.printerStartBasic.z, true) < Config.DrawDistance) then
          if IsInteriorPropEnabled(businessConfig.interiorID, 'counterfeit_standard_equip_no_prod') and not IsInteriorPropEnabled(businessConfig.interiorID, 'counterfeit_setup') then
            DrawMarker(Config.MarkerType, zones.printerStartBasic.x, zones.printerStartBasic.y, zones.printerStartBasic.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 100, false, true, 2, false, false, false, false)
          end
        end

        --printer start (upgrade)
        if (not business.disabled and GetDistanceBetweenCoords(coords, zones.printerStartUpgrade.x, zones.printerStartUpgrade.y, zones.printerStartUpgrade.z, true) < Config.DrawDistance) then
          if IsInteriorPropEnabled(businessConfig.interiorID, 'counterfeit_upgrade_equip_no_prod') and not IsInteriorPropEnabled(businessConfig.interiorID, 'counterfeit_setup') then
            DrawMarker(Config.MarkerType, zones.printerStartUpgrade.x, zones.printerStartUpgrade.y, zones.printerStartUpgrade.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 100, false, true, 2, false, false, false, false)
          end
        end

        --printer end
        if (not business.disabled and GetDistanceBetweenCoords(coords, zones.printerTake.x, zones.printerTake.y, zones.printerTake.z, true) < Config.DrawDistance) then
          if (IsInteriorPropEnabled(businessConfig.interiorID, 'counterfeit_standard_equip') or IsInteriorPropEnabled(businessConfig.interiorID, 'counterfeit_upgrade_equip')) and IsInteriorPropEnabled(businessConfig.interiorID, 'counterfeit_setup') then
            DrawMarker(Config.MarkerType, zones.printerTake.x, zones.printerTake.y, zones.printerTake.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 100, false, true, 2, false, false, false, false)
          end
        end

        --Dryers
        for k,pos in pairs(zones.dryers) do
          local i = k
          if (not business.disabled and GetDistanceBetweenCoords(coords, pos.x, pos.y, pos.z, true) < Config.DrawDistance) then
            if getDryerState(business, businessConfig.dryerLetters[i]) == "Off" or getDryerState(business, businessConfig.dryerLetters[i]) == "Open" then
              DrawMarker(Config.MarkerType, pos.x, pos.y, pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 100, false, true, 2, false, false, false, false)
            end
          end
        end

        for k,pos in pairs(zones.production) do
          if (not business.disabled and GetDistanceBetweenCoords(coords, pos.x, pos.y, pos.z, true) < Config.DrawDistance) then
            if not IsInteriorPropEnabled(businessConfig.interiorID, 'counterfeit_cashpile100d') then
              DrawMarker(Config.MarkerType, pos.x, pos.y, pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 100, false, true, 2, false, false, false, false)
            end
          end
        end
      end
    end
  end
end)


-- Enter / Exit marker events
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(0)

    local coords          = GetEntityCoords(GetPlayerPed(-1))
    local interiorID      = GetInteriorFromEntity(PlayerPedId())
    local isInMarker      = false
    local currentBusiness = nil
    local currentPart     = nil

    --Meth Heist Zones
    for k,pos in pairs(Config.HeistZones.chemicalPlant) do
      if (GetDistanceBetweenCoords(coords, pos.x, pos.y, pos.z, true) < Config.MarkerSize.x) then
        isInMarker      = true
        currentBusiness = 'none'
        currentPart     = 'chemicalPlant_Heist'
      end
    end

    if (GetDistanceBetweenCoords(coords, 2664.12, 1642.01, 23.87, true) < Config.MarkerSize.x) and PlayerData.job.name == 'police' then
      isInMarker      = true
      currentBusiness = 'none'
      currentPart     = 'chemicalPlant_Reset'
    end

    for i=1, #Config.Businesses, 1 do
      local business = Config.Businesses[i]

      --entering (the outside portal)
      if (not business.disabled and GetDistanceBetweenCoords(coords, business.entering.x, business.entering.y, business.entering.z, true) < Config.MarkerSize.x) then
        isInMarker      = true
        currentBusiness = business.name
        currentPart     = 'entering'
      end

      if (not business.disabled and GetDistanceBetweenCoords(coords, business.outside.x, business.outside.y, business.outside.z, true) < Config.MarkerSize.x) then
        isInMarker      = true
        currentBusiness = business.name
        currentPart     = 'outside'
      end

      if Config.BusinessSetup.Weed_Farm.interiorID == interiorID then
        local businessConfig = Config.BusinessSetup.Weed_Farm
        local zones = businessConfig.zones

        --room menu portal
        if (HasChest and not business.disabled and GetDistanceBetweenCoords(coords, zones.roomMenu.x, zones.roomMenu.y, zones.roomMenu.z, true) < Config.MarkerSize.x) then
          isInMarker      = true
          currentBusiness = business.name
          currentPart     = 'roomMenu'
        end

        --exit (the inside portal)
        if (not business.disabled and GetDistanceBetweenCoords(coords, zones.exit.x, zones.exit.y, zones.exit.z, true) < Config.MarkerSize.x) then
          isInMarker      = true
          currentBusiness = business.name
          currentPart     = 'exit'
        end

        for k,pos in pairs(zones.planters) do
          local i = k
          if (not business.disabled and GetDistanceBetweenCoords(coords, pos.x, pos.y, pos.z) < Config.MarkerSize.x) then
            if IsInteriorPropEnabled(businessConfig.interiorID, 'weed_set_up') or IsInteriorPropEnabled(businessConfig.interiorID, 'weed_upgrade_equip') then
              if getGrowStage(businessConfig.planterLetters[i]) == 0 or getGrowStage(businessConfig.planterLetters[i]) == 3 then
                isInMarker      = true
                currentBusiness = business.name
                currentPart     = string.format("planter_%s", businessConfig.planterLetters[i])
              end
            end
          end
        end

        --weed drying marker
        if (not business.disabled and GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1), false), 1040.81, -3202.33, -39.16) < Config.MarkerSize.x) then
          if not IsInteriorPropEnabled(businessConfig.interiorID, 'weed_drying') then
            isInMarker      = true
            currentBusiness = business.name
            currentPart     = 'weedDrying'
          end
        end

        --weed production markers
        for k,pos in pairs(zones.production) do
          if (not business.disabled and GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1), false), pos.x, pos.y, pos.z) < Config.MarkerSize.x) then
            if not IsInteriorPropEnabled(businessConfig.interiorID, 'weed_production') then
              isInMarker      = true
              currentBusiness = business.name
              currentPart     = 'weedProcess'
            end
          end
        end
      end


      if Config.BusinessSetup.Cocaine_Lockup.interiorID == interiorID then
        local businessConfig = Config.BusinessSetup.Cocaine_Lockup
        local zones = businessConfig.zones

        --room menu portal
        if (HasChest and not business.disabled and GetDistanceBetweenCoords(coords, zones.roomMenu.x, zones.roomMenu.y, zones.roomMenu.z, true) < Config.MarkerSize.x) then
          isInMarker      = true
          currentBusiness = business.name
          currentPart     = 'roomMenu'
        end

        --exit (the inside portal)
        if (not business.disabled and GetDistanceBetweenCoords(coords, zones.exit.x, zones.exit.y, zones.exit.z, true) < Config.MarkerSize.x) then
          isInMarker      = true
          currentBusiness = business.name
          currentPart     = 'exit'
        end

        for k,pos in pairs(zones.production) do
          local i = k
          if (not business.disabled and GetDistanceBetweenCoords(coords, pos.x, pos.y, pos.z) < Config.MarkerSize.x) then
            if IsInteriorPropEnabled(businessConfig.interiorID, 'table_equipment') or IsInteriorPropEnabled(businessConfig.interiorID, 'table_equipment_upgrade') then
              if not isCokeCut(businessConfig.cokeNumbers[i]) then
                isInMarker      = true
                currentBusiness = business.name
                currentPart     = string.format("coke_%s", businessConfig.cokeNumbers[i])
                --print("\ncurrentPart: " .. currentPart)
              end
            end
          end
        end
        for k,pos in pairs(zones.production_upgrade) do
          local i = k + 3 --offset
          if (not business.disabled and GetDistanceBetweenCoords(coords, pos.x, pos.y, pos.z) < Config.MarkerSize.x) then
            if IsInteriorPropEnabled(businessConfig.interiorID, 'table_equipment_upgrade') then
              if not isCokeCut(businessConfig.cokeNumbers[i]) then
                isInMarker      = true
                currentBusiness = business.name
                currentPart     = string.format("coke_%s", businessConfig.cokeNumbers[i])
                --print("\ncurrentPart: " .. currentPart)
              end
            end
          end
        end
      end


      if Config.BusinessSetup.Meth_Lab.interiorID == interiorID then
        local businessConfig = Config.BusinessSetup.Meth_Lab
        local zones = businessConfig.zones

        --room menu portal
        if (HasChest and not business.disabled and GetDistanceBetweenCoords(coords, zones.roomMenu.x, zones.roomMenu.y, zones.roomMenu.z, true) < Config.MarkerSize.x) then
          isInMarker      = true
          currentBusiness = business.name
          currentPart     = 'roomMenu'
        end

        --exit (the inside portal)
        if (not business.disabled and GetDistanceBetweenCoords(coords, zones.exit.x, zones.exit.y, zones.exit.z, true) < Config.MarkerSize.x) then
          isInMarker      = true
          currentBusiness = business.name
          currentPart     = 'exit'
        end

        --meth production
        if (not business.disabled and GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1), false), zones.production.x, zones.production.y, zones.production.z) < Config.MarkerSize.x) then
          isInMarker      = true
          currentBusiness = business.name
          currentPart     = 'methProduction'
        end

        --meth processing
        if (not business.disabled and GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1), false), zones.processing.x, zones.processing.y, zones.processing.z) < Config.MarkerSize.x) then
          if not IsInteriorPropEnabled(businessConfig.interiorID, 'meth_lab_production') then
            isInMarker      = true
            currentBusiness = business.name
            currentPart     = 'methProcess'
          end
        end
      end


      if Config.BusinessSetup.Doc_Forge.interiorID == interiorID then
        local businessConfig = Config.BusinessSetup.Doc_Forge
        local zones = businessConfig.zones

        --room menu portal
        if (HasChest and not business.disabled and GetDistanceBetweenCoords(coords, zones.roomMenu.x, zones.roomMenu.y, zones.roomMenu.z, true) < Config.MarkerSize.x) then
          isInMarker      = true
          currentBusiness = business.name
          currentPart     = 'roomMenu'
        end

        --exit (the inside portal)
        if (not business.disabled and GetDistanceBetweenCoords(coords, zones.exit.x, zones.exit.y, zones.exit.z, true) < Config.MarkerSize.x) then
          isInMarker      = true
          currentBusiness = business.name
          currentPart     = 'exit'
        end

        for k,pos in pairs(zones.production) do
          if (not business.disabled and GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1), false), pos.x, pos.y, pos.z) < Config.MarkerSize.x) then
            if not IsInteriorPropEnabled(businessConfig.interiorID, 'production') then
              isInMarker      = true
              currentBusiness = business.name
              currentPart     = 'production'
            end
          end
        end
      end


      if Config.BusinessSetup.Money_Wash.interiorID == interiorID then
        local businessConfig = Config.BusinessSetup.Money_Wash
        local zones = businessConfig.zones

        --room menu portal
        if (HasChest and not business.disabled and GetDistanceBetweenCoords(coords, zones.roomMenu.x, zones.roomMenu.y, zones.roomMenu.z, true) < Config.MarkerSize.x) then
          isInMarker      = true
          currentBusiness = business.name
          currentPart     = 'roomMenu'
        end

        --exit (the inside portal)
        if (not business.disabled and GetDistanceBetweenCoords(coords, zones.exit.x, zones.exit.y, zones.exit.z, true) < Config.MarkerSize.x) then
          isInMarker      = true
          currentBusiness = business.name
          currentPart     = 'exit'
        end

        --printer start (basic)
        if (not business.disabled and GetDistanceBetweenCoords(coords, zones.printerStartBasic.x, zones.printerStartBasic.y, zones.printerStartBasic.z, true) < Config.MarkerSize.x) then
          if IsInteriorPropEnabled(businessConfig.interiorID, 'counterfeit_standard_equip_no_prod') and not IsInteriorPropEnabled(businessConfig.interiorID, 'counterfeit_setup') then
            isInMarker      = true
            currentBusiness = business.name
            currentPart     = 'printerStart'
          end
        end

        --printer start (upgrade)
        if (not business.disabled and GetDistanceBetweenCoords(coords, zones.printerStartUpgrade.x, zones.printerStartUpgrade.y, zones.printerStartUpgrade.z, true) < Config.MarkerSize.x) then
          if IsInteriorPropEnabled(businessConfig.interiorID, 'counterfeit_upgrade_equip_no_prod') and not IsInteriorPropEnabled(businessConfig.interiorID, 'counterfeit_setup') then
            isInMarker      = true
            currentBusiness = business.name
            currentPart     = 'printerStart'
          end
        end

        --printer end
        if (not business.disabled and GetDistanceBetweenCoords(coords, zones.printerTake.x, zones.printerTake.y, zones.printerTake.z, true) < Config.MarkerSize.x) then
          if (IsInteriorPropEnabled(businessConfig.interiorID, 'counterfeit_standard_equip') or IsInteriorPropEnabled(businessConfig.interiorID, 'counterfeit_upgrade_equip')) and IsInteriorPropEnabled(businessConfig.interiorID, 'counterfeit_setup') then
            isInMarker      = true
            currentBusiness = business.name
            currentPart     = 'printerTake'
          end
        end

        for k,pos in pairs(zones.dryers) do
          local i = k
          if (not business.disabled and GetDistanceBetweenCoords(coords, pos.x, pos.y, pos.z) < Config.MarkerSize.x) then
            if getDryerState(business, businessConfig.dryerLetters[i]) == "Off" or getDryerState(business, businessConfig.dryerLetters[i]) == "Open" then
              isInMarker      = true
              currentBusiness = business.name
              currentPart     = string.format("dryer_%s", businessConfig.dryerLetters[i])
            end
          end
        end

        for k,pos in pairs(zones.production) do
          if (not business.disabled and GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1), false), pos.x, pos.y, pos.z) < Config.MarkerSize.x) then
            if not IsInteriorPropEnabled(businessConfig.interiorID, 'counterfeit_cashpile100d') then
              isInMarker      = true
              currentBusiness = business.name
              currentPart     = 'production'
            end
          end
        end
      end
    end

    if isInMarker and not HasAlreadyEnteredMarker or (isInMarker and (LastBusiness ~= currentBusiness or LastPart ~= currentPart)) then
      HasAlreadyEnteredMarker = true
      LastBusiness            = currentBusiness
      LastPart                = currentPart

      print("\nEnter/Exit marker events currentBusiness: " .. currentBusiness)
      print("\nEnter/Exit marker events currentPart: " .. currentPart)
      TriggerEvent('esx_business:hasEnteredMarker', currentBusiness, currentPart)
    end

    if not isInMarker and HasAlreadyEnteredMarker then
      HasAlreadyEnteredMarker = false
      TriggerEvent('esx_business:hasExitedMarker', LastBusiness, LastPart)
    end
  end
end)


-- Key controls
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(0)

    local interiorID = GetInteriorFromEntity(PlayerPedId())

    if CurrentAction ~= nil then
      ESX.ShowHelpNotification(CurrentActionMsg)

      if IsControlJustReleased(0, Keys['E']) then
        --meth heist
        if CurrentAction == 'chemical_plant_heist' then
          ESX.TriggerServerCallback('esx_business:canStartChemicalHeist', function(canStart)
            if canStart then
              TriggerEvent('esx_business:chemicalHeistStart')
            end
          end, coords)
        elseif CurrentAction == 'chemical_plant_reset' then
          TriggerServerEvent('esx_business:disableAlarm')
        end


        --Businesses
        if CurrentAction == 'business_menu' then
          --print("\nsending business: " .. CurrentActionData.business.name .. " to OpenBusinessMenu")
          OpenBusinessMenu(CurrentActionData.business)
        elseif CurrentAction == 'room_menu' then -- this is triggering 3 times?
          --print("\nsending business: " .. CurrentActionData.business.name .. " to OpenRoomMenu")
          OpenRoomMenu(CurrentActionData.business, CurrentActionData.owner)
        elseif CurrentAction == 'room_exit' then
          TriggerEvent('instance:leave')
        end


        if Config.BusinessSetup.Weed_Farm.interiorID == interiorID then
          local businessConfig = Config.BusinessSetup.Weed_Farm

          if CurrentAction == 'weed_drying' then
            ESX.TriggerServerCallback('esx_business:canHangWeed', function(canStart)
              if canStart then
                TriggerEvent('esx_business:hangWeedStart', CurrentActionData.business, hostThisInstance)
              end
            end)
          end

          if CurrentAction == 'weed_process' then
            ESX.TriggerServerCallback('esx_business:canProcessWeed', function(canStart)
              if canStart then
                TriggerEvent('esx_business:processWeedStart', CurrentActionData.business, hostThisInstance)
              end
            end)
          end

          for i,name in ipairs(businessConfig.planterSetupVars) do
            if CurrentAction == name then
              ESX.TriggerServerCallback('esx_business:canGrowWeed', function(canStart)
                if canStart then
                  TriggerEvent('esx_business:weedGrowStart', CurrentActionData.business, hostThisInstance, businessConfig.planterLetters[i])
                end
              end)
            end
          end

          for i,name in ipairs(businessConfig.planterHarvestVars) do
            if CurrentAction == name then
              ESX.TriggerServerCallback('esx_business:canHarvestWeed', function(canStart)
                if canStart then
                  TriggerEvent('esx_business:harvestWeedStart', CurrentActionData.business, hostThisInstance, businessConfig.planterLetters[i])
                end
              end)
            end
          end
        end


        if Config.BusinessSetup.Cocaine_Lockup.interiorID == interiorID then
          local businessConfig = Config.BusinessSetup.Cocaine_Lockup
          for i,name in ipairs(businessConfig.cokeSetupVars) do
            if CurrentAction == name then
              ESX.TriggerServerCallback('esx_business:canProcessCoke', function(canStart)
                if canStart then
                  TriggerEvent('esx_business:processCokeStart', CurrentActionData.business, hostThisInstance, businessConfig.cokeNumbers[i])
                end
              end)
            end
          end
        end


        if Config.BusinessSetup.Meth_Lab.interiorID == interiorID then
          local businessConfig = Config.BusinessSetup.Meth_Lab

          if CurrentAction == 'meth_production' then
            ESX.TriggerServerCallback('esx_business:canProduceMeth', function(canStart)
              if canStart then
                TriggerEvent('esx_business:produceMethStart', CurrentActionData.business, hostThisInstance)
              end
            end)
          end

          if CurrentAction == 'meth_process' then
            ESX.TriggerServerCallback('esx_business:canProcessMeth', function(canStart)
              if canStart then
                TriggerEvent('esx_business:processMethStart', CurrentActionData.business, hostThisInstance)
              end
            end)
          end
        end


        if Config.BusinessSetup.Doc_Forge.interiorID == interiorID then
          local businessConfig = Config.BusinessSetup.Doc_Forge
        end


        if Config.BusinessSetup.Money_Wash.interiorID == interiorID then
          local businessConfig = Config.BusinessSetup.Money_Wash

          for i,name in ipairs(businessConfig.dryerSetupVars) do
            if CurrentAction == name then
              ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'amount_to_wash', {
                title = _U('amount'),
              }, function(data2, menu)

                local quantity = tonumber(data2.value)
                if quantity == nil then
                  ESX.ShowNotification(_U('amount_invalid'))
                else
                  menu.close()
                  ESX.TriggerServerCallback('esx_business:canWashMoney', function(canStart)
                    if canStart then
                      TriggerEvent('esx_business:moneyWashStart', CurrentActionData.business, hostThisInstance, businessConfig.dryerLetters[i], quantity)
                    end
                  end, quantity)
                end
              end, function(data2,menu)
                menu.close()
              end)
            end
          end

          for i,dryerTakeVar in ipairs(businessConfig.dryerTakeVars) do
            if CurrentAction == dryerTakeVar then
              TriggerEvent('esx_business:takeWashedMoneyStart', CurrentActionData.business, hostThisInstance, businessConfig.dryerLetters[i])
            end
          end

          if CurrentAction == 'printer_start' then
            ESX.TriggerServerCallback('esx_business:canPrintMoney', function(canStart)
              if canStart then
                TriggerEvent('esx_business:moneyPrintStart', CurrentActionData.business, hostThisInstance)
              end
            end)
          end

          if CurrentAction == 'printer_take' then
            ESX.TriggerServerCallback('esx_business:canTakePrintedMoney', function(canStart)
              if canStart then
                TriggerEvent('esx_business:takePrintedMoneyStart', CurrentActionData.business, hostThisInstance)
              end
            end)
          end

          if CurrentAction == 'money_production' then
            ESX.TriggerServerCallback('esx_business:canProduceDirtyMoney', function(canStart)
              if canStart then
                TriggerEvent('esx_business:produceDirtyMoneyStart', CurrentActionData.business, hostThisInstanc)
              end
            end)
          end
        end

        CurrentAction = nil
      end
    else -- no current action, sleep mode
      Citizen.Wait(500)
    end
  end
end)
