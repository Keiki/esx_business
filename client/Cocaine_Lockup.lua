local Keys = {
  ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
  ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
  ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
  ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
  ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
  ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
  ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
  ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
  ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

ESX                           = nil
local isDoingAction = false

function isCokeCut(propNumber)
  local businessConfig = Config.BusinessSetup.Cocaine_Lockup
  local prop = string.format("coke_cut_%s", propNumber)

  if IsInteriorPropEnabled(businessConfig.interiorID, prop) then
    return true
  else
    return false
  end
end


--Action Blocking
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(1)

    if isDoingAction then
      DisableControlAction(0, Keys['F1'], true) -- Disable phone
      DisableControlAction(0, Keys['F2'], true) -- Inventory
      DisableControlAction(0, Keys['F3'], true) -- Animations
      DisableControlAction(0, Keys['F6'], true) -- Job
      DisableControlAction(0, Keys['Enter'], true)
      DisableControlAction(0, Keys['E'], true)
    else
      Citizen.Wait(500)
    end
  end
end)


RegisterNetEvent('esx_business:processCokeStart')
AddEventHandler('esx_business:processCokeStart', function(business, hostThisInstance, cokeNumber)
  print("\ncokeNumber: " ..cokeNumber)
  local ped = PlayerPedId()
  local oldCoords = GetEntityCoords(ped)
  local duration = Config.TimeToProcessCoke
  local timer = 0
  local failed = false

  local coords = GetEntityCoords(PlayerPedId())
  local interiorID = GetInteriorAtCoords(coords)
  local scenario = 'PROP_HUMAN_BUM_BIN'
  TaskStartScenarioInPlace(PlayerPedId(), scenario, 0, false)

  TriggerServerEvent('esx_business:enableProp', string.format("coke_cut_%s", cokeNumber), hostThisInstance, business)

  while IsPedUsingScenario(ped,scenario) == false do
    Wait(0)
  end

  while failed == false do
    isDoingAction = true
    timer = timer + 1
    local coords = GetEntityCoords(ped)
    local isDead = IsEntityDead(ped)
    local movedAway = GetDistanceBetweenCoords(oldCoords, coords.x, coords.y, coords.z, true) > Config.MarkerSize.x
    local cancelledAnim = IsPedUsingScenario(ped,scenario) == false

    if isDead or movedAway or cancelledAnim then
      isDoingAction = false
      ESX.ShowNotification("The action was cancelled")
      failed = true
      ClearPedTasksImmediately(PlayerPedId())
      TriggerServerEvent('esx_business:disableProp', string.format("coke_cut_%s", cokeNumber), hostThisInstance, business)
      break
    end

    if timer == duration then
      isDoingAction = false
      ClearPedTasksImmediately(PlayerPedId())
      TriggerServerEvent('esx_business:processCokeDone', business, hostThisInstance)
      TriggerServerEvent('esx_business:disableProp', string.format("coke_cut_%s", cokeNumber), hostThisInstance, business)
      break
    end

    Wait(1000)
  end
end)
