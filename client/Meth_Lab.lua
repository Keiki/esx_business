local Keys = {
  ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
  ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
  ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
  ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
  ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
  ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
  ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
  ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
  ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

local alarmReset      = false
local AlarmSoundId    = nil
local isDoingAction   = false
local security_guards = {} -- for triggering hostile
local spawnedNpcs     = {}
local Npcs = {
  {x=2671.23, y=1591.22, z=28.86, h=356},
  {x=2688.37, y=1593.18, z=31.51, h=356},
  {x=2671.18, y=1607.38, z=23.5, h=273},
  {x=2729.79, y=1526.87, z=31.51, h=71},
  {x=2735.33, y=1458.36, z=29.45, h=74},
  {x=2720.15, y=1469.86, z=33.31, h=110},
  {x=2667.46, y=1372.97, z=23.05, h=315},
  {x=2675.68, y=1458.12, z=23.5, h=0}
}

DecorRegister('isSurrendering', 3) -- 3 for int
DecorRegister('isCuffed', 3) -- 3 for int


function clearArea()
  for _, npc in pairs(Npcs) do
    local coords = {x=npc.x, y=npc.y, z=npc.z}
    ClearArea(coords.x, coords.y, coords.z, 2.0, true, false, false, false);
    ClearAreaOfCops(coords.x, coords.y, coords.z, 2.0, 1)
    ClearAreaOfPeds(coords.x, coords.y, coords.z, 2.0, 1)
    ClearAreaOfEverything(coords.x, coords.y, coords.z, 2.0, true, true, true, true)
  end
end

function createGuards()
  clearArea()
  for _, npc in pairs(Npcs) do
    local coords = {x=npc.x, y=npc.y, z=npc.z}
    local hash = GetHashKey("mp_s_m_armoured_01")
    --if not DoesObjectOfTypeExistAtCoords(coords.x, coords.y, coords.z, 2.0, hash, 0) then
      TriggerEvent('esx_business:createGuard', coords, npc.h)
    --end
  end
end


local entityEnumerator = {
  __gc = function(enum)
    if enum.destructor and enum.handle then
      enum.destructor(enum.handle)
    end
    enum.destructor = nil
    enum.handle = nil
  end
}

local function EnumerateEntities(initFunc, moveFunc, disposeFunc)
  return coroutine.wrap(function()
    local iter, id = initFunc()
    if not id or id == 0 then
      disposeFunc(iter)
      return
    end

    local enum = {handle = iter, destructor = disposeFunc}
    setmetatable(enum, entityEnumerator)

    local next = true
    repeat
      coroutine.yield(id)
      next, id = moveFunc(iter)
    until not next

    enum.destructor, enum.handle = nil, nil
    disposeFunc(iter)
  end)
end

function EnumeratePeds()
  return EnumerateEntities(FindFirstPed, FindNextPed, EndFindPed)
end

function triggerCombat()
  local player = GetPlayerPed(-1)
  local playerCoords = GetEntityCoords(player)

  for ped in EnumeratePeds() do
    if ped ~= player then
      local pedCoords = GetEntityCoords(ped)
      local groupHash = GetPedRelationshipGroupHash(ped)
      local isCuffed = DecorGetInt(ped, 'isCuffed') == 1
      local isSurrendering = DecorGetInt(ped, 'isCuffed') == 1

      if not (isCuffed and isSurrendering) and (groupHash == GetHashKey("SECURITY_GUARD")) and HasEntityClearLosToEntity(ped, player, 17) then
        SetPedAlertness(ped, 3)
        TaskCombatPed(ped, player, 0, 16 )
      end
    end
  end
end


Citizen.CreateThread(function()
  while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end

	while ESX.GetPlayerData().job == nil do
		Citizen.Wait(10)
	end

	PlayerData = ESX.GetPlayerData()
  createGuards()
end)


Citizen.CreateThread(function()
	local blip = AddBlipForCoord(Config.Blips.ChemicalPlant.coords.x, Config.Blips.ChemicalPlant.coords.y,Config.Blips.ChemicalPlant.coords.z)
	SetBlipSprite(blip, 439)
	SetBlipScale(blip, 0.8)
	SetBlipAsShortRange(blip, true)
	BeginTextCommandSetBlipName("STRING")
	AddTextComponentString(Config.Blips.ChemicalPlant.name)
	EndTextCommandSetBlipName(blip)
end)


--Action Blocking
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(1)

    if isDoingAction then
      DisableControlAction(0, Keys['F1'], true) -- Disable phone
      DisableControlAction(0, Keys['F2'], true) -- Inventory
      DisableControlAction(0, Keys['F3'], true) -- Animations
      DisableControlAction(0, Keys['F6'], true) -- Job
      DisableControlAction(0, Keys['Enter'], true)
      DisableControlAction(0, Keys['E'], true)
    else
      Citizen.Wait(500)
    end
  end
end)

AddEventHandler('onResourceStop', function(resource)
	if resource ~= GetCurrentResourceName() then return end
	for i=1, #spawnedNpcs, 1 do
    local ped = spawnedNpcs[i][1]
    local coords = spawnedNpcs[i][2]
    --print(ped)
    --print(json.encode(coords))
    SetEntityAsMissionEntity(ped)
    DeleteEntity(ped)
  end
end)

RegisterNetEvent('esx_business:createGuard')
AddEventHandler('esx_business:createGuard', function(coords, h)
  local hash = GetHashKey("mp_s_m_armoured_01")

  while not HasModelLoaded(hash) do
    RequestModel(hash)
    Wait(20)
  end

  if NetworkIsHost() then
    --if not DoesObjectOfTypeExistAtCoords(coords.x, coords.y, coords.z, 2, hash, 0) then
      local ped = CreatePed(6, hash, coords.x, coords.y, coords.z, h, true, true)
      SetBlockingOfNonTemporaryEvents(ped, false)
      SetPedAlertness(ped, 2)
      TaskStandGuard(ped, coords.x, coords.y, coords.z, h, "WORLD_HUMAN_GUARD_STAND")
      --SetPedAsEnemy(ped, true)
      SetPedCombatAbility(ped, 0)
      SetPedCombatAttributes(ped, 46, true)
      SetPedFleeAttributes(ped, 0, 0)
      GiveWeaponToPed(ped, GetHashKey("weapon_pistol"), 20, false)
      --SetPedCanBeTargetted(ped, false)
      SetPedRelationshipGroupHash(ped, GetHashKey("SECURITY_GUARD"))
      table.insert(spawnedNpcs,{ped,coords}) -- wrong number of args
      Wait(500)
    --end
  end
end)


RegisterNetEvent('esx_business:chemicalHeistStart')
AddEventHandler('esx_business:chemicalHeistStart', function(randomItem)
  triggerCombat()

  local scenario = 'PROP_HUMAN_BUM_BIN'
  TaskStartScenarioInPlace(PlayerPedId(), scenario, 0, false)

  SetPlayerWantedLevel(PlayerId(), 4, false)
  SetPlayerWantedLevelNow(PlayerId(), false)

  local ped = PlayerPedId()
  local oldCoords = GetEntityCoords(ped)
  local duration = Config.TimeToExtract
  local timer = 0
  local failed = false

  while IsPedUsingScenario(ped,scenario) == false do
    Wait(0)
  end

  while failed == false do
		isDoingAction = true
    timer = timer + 1
    local coords = GetEntityCoords(ped)
    local isDead = IsEntityDead(ped)
    local movedAway = GetDistanceBetweenCoords(oldCoords, coords.x, coords.y, coords.z, true) > Config.MarkerSize.x
    local cancelledAnim = IsPedUsingScenario(ped,scenario) == false

    if isDead or movedAway or cancelledAnim then
			isDoingAction = false
      ESX.ShowNotification("The action was cancelled")
      ClearPedTasksImmediately(PlayerPedId())
      failed = true
      break
    end

    if timer == duration then
			isDoingAction = false
      ClearPedTasksImmediately(PlayerPedId())
      TriggerServerEvent('esx_business:chemicalHeistDone', randomItem)
      break
    end

    Wait(1000)
  end
end)


RegisterNetEvent('esx_business:killblip')
AddEventHandler('esx_business:killblip', function()
    RemoveBlip(blipRobbery)
end)

RegisterNetEvent('esx_business:setblip')
AddEventHandler('esx_business:setblip', function(coords)
  blipRobbery = AddBlipForCoord(coords.x, coords.y, coords.z)
  SetBlipSprite(blipRobbery , 161)
  SetBlipScale(blipRobbery , 2.0)
  SetBlipColour(blipRobbery, 3)
  PulseBlip(blipRobbery)
end)

RegisterNetEvent('esx_business:killAlarm')
AddEventHandler('esx_business:killAlarm', function()
  StopSound(AlarmSoundId)
  ReleaseSoundId(AlarmSoundId)
  AlarmSoundId = nil
  alarmReset = true
end)

RegisterNetEvent('esx_business:triggerAlarm')
AddEventHandler('esx_business:triggerAlarm', function()
  while true do
    if not alarmReset then
      local AlarmSoundId = GetSoundId()
      local coords = Config.CircleZones.ChemicalPlant.coords
      PlaySoundFromCoord(AlarmSoundId, "scanner_alarm_os", coords.x, coords.y, coords.z, "dlc_xm_iaa_player_facility_sounds", 1, 250, 0)
    else
      StopSound(-1)
      break
    end

    Wait(1000)
  end
end)


RegisterNetEvent('esx_business:produceMethStart')
AddEventHandler('esx_business:produceMethStart', function(business, hostThisInstance)
	local scenario = 'PROP_HUMAN_BUM_BIN'
  TaskStartScenarioInPlace(PlayerPedId(), scenario, 0, false)

  local ped = PlayerPedId()
  local oldCoords = GetEntityCoords(ped)
  local duration = Config.TimeToAnim
  local timer = 0
  local failed = false

  while IsPedUsingScenario(ped,scenario) == false do
    Wait(0)
  end

  while failed == false do
		isDoingAction = true
    timer = timer + 1
    local coords = GetEntityCoords(ped)
    local isDead = IsEntityDead(ped)
    local movedAway = GetDistanceBetweenCoords(oldCoords, coords.x, coords.y, coords.z, true) > Config.MarkerSize.x
    local cancelledAnim = IsPedUsingScenario(ped,scenario) == false

    if isDead or movedAway or cancelledAnim then
			isDoingAction = false
      ESX.ShowNotification("The action was cancelled")
      failed = true
      ClearPedTasksImmediately(PlayerPedId())
      break
    end

    if timer == duration then
			isDoingAction = false
      ClearPedTasksImmediately(PlayerPedId())
			local sleepTime = (1000 * 60) * Config.TimeToCookMethBasic
      TriggerServerEvent('esx_business:produceMethDone', business, hostThisInstance, sleepTime)
			local string = string.format("Estimated Time: %s minutes", sleepTime/60000)
      ESX.ShowHelpNotification(string)
      break
    end

    Wait(1000)
  end
end)

RegisterNetEvent('esx_business:processMethStart')
AddEventHandler('esx_business:processMethStart', function(business, hostThisInstance)
  local ped = PlayerPedId()
  local oldCoords = GetEntityCoords(ped)
	local interiorID = GetInteriorAtCoords(oldCoords)
  local duration = Config.TimeToProcessMeth
  local timer = 0
  local failed = false

  local scenario = 'PROP_HUMAN_BUM_BIN'
  TaskStartScenarioInPlace(PlayerPedId(), scenario, 0, false)

  TriggerServerEvent('esx_business:enableProp', 'meth_lab_production', hostThisInstance, business)

  while IsPedUsingScenario(ped,scenario) == false do
    Wait(0)
  end

  while failed == false do
		isDoingAction = true
    timer = timer + 1
    local coords = GetEntityCoords(ped)
    local isDead = IsEntityDead(ped)
    local movedAway = GetDistanceBetweenCoords(oldCoords, coords.x, coords.y, coords.z, true) > Config.MarkerSize.x
    local cancelledAnim = IsPedUsingScenario(ped,scenario) == false

    if isDead or movedAway or cancelledAnim then
			isDoingAction = false
      ESX.ShowNotification("The action was cancelled")
      failed = true
      ClearPedTasksImmediately(PlayerPedId())
      TriggerServerEvent('esx_business:disableProp', 'meth_lab_production', hostThisInstance, business)
      break
    end

    if timer == duration then
			isDoingAction = true
      ClearPedTasksImmediately(PlayerPedId())
			local amount = nil
			if IsInteriorPropEnabled(interiorID, 'meth_lab_basic') then
				amount = Config.GiveUnprocessedMethBasic
      elseif IsInteriorPropEnabled(interiorID, 'meth_lab_upgrade') then
				amount = Config.GiveUnprocessedMethUpgrade
      end

      TriggerServerEvent('esx_business:disableProp', 'meth_lab_production', hostThisInstance, business)
      TriggerServerEvent('esx_business:processMethDone', business, hostThisInstance, amount)
      break
    end

    Wait(1000)
  end
end)
