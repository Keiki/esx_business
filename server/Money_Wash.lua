ESX.RegisterServerCallback('esx_business:canPrintMoney', function(source, cb)
  local xPlayer = ESX.GetPlayerFromId(source) --attempt to index nil vlalue
  local money_wash_plates = xPlayer.getInventoryItem('money_wash_plates')
  local money_wash_blank_notes = xPlayer.getInventoryItem('money_wash_blank_notes')
  local money_wash_plates_count = xPlayer.getInventoryItem('money_wash_plates').count
  local money_wash_blank_notes_count = xPlayer.getInventoryItem('money_wash_blank_notes').count

  if money_wash_plates_count < 1 then
    TriggerClientEvent('esx:showNotification', source, _U('not_enough_items', 1, money_wash_plates.label))
    cb(false)
  elseif money_wash_blank_notes_count < 1 then
    TriggerClientEvent('esx:showNotification', source, _U('not_enough_items', 1, money_wash_blank_notes.label))
    cb(false)
  else
    TriggerClientEvent('esx:showNotification', source, _U('action_start'))
    cb(true)
  end
end)

RegisterServerEvent('esx_business:moneyPrintStart')
AddEventHandler('esx_business:moneyPrintStart', function(business, hostThisInstance)
  local xPlayer = ESX.GetPlayerFromId(source) --attempt to index nil vlalue
  local money_wash_plates = xPlayer.getInventoryItem('money_wash_plates')
  local money_wash_blank_notes = xPlayer.getInventoryItem('money_wash_blank_notes')
  local money_wash_plates_count = xPlayer.getInventoryItem('money_wash_plates').count
  local money_wash_blank_notes_count = xPlayer.getInventoryItem('money_wash_blank_notes').count

  if money_wash_plates_count < 1 then
    TriggerClientEvent('esx:showNotification', source, _U('not_enough_items', 1, money_wash_plates.label))
  elseif money_wash_blank_notes_count < 1 then
    TriggerClientEvent('esx:showNotification', source, _U('not_enough_items', 1, money_wash_blank_notes.label))
  else
    TriggerClientEvent('esx_business:moneyPrintStart', source, business, hostThisInstance)
    TriggerClientEvent('esx:showNotification', source, _U('action_start'))
  end
end)

RegisterServerEvent('esx_business:moneyPrintDone')
AddEventHandler('esx_business:moneyPrintDone', function(business, hostThisInstance, sleepTime, printerType)
  local xPlayer = ESX.GetPlayerFromId(source) --attempt to index nil vlalue

  xPlayer.removeInventoryItem('money_wash_blank_notes', 1)
  TriggerClientEvent('esx:showNotification', source, _U('action_done'))

  if printerType == 'standard' then
    TriggerEvent('esx_business:disableProp', 'counterfeit_standard_equip_no_prod', hostThisInstance, business)
    Wait(1000)
    TriggerEvent('esx_business:enableProp', 'counterfeit_standard_equip', hostThisInstance, business)
  elseif printerType == 'upgrade' then
    TriggerEvent('esx_business:disableProp', 'counterfeit_upgrade_equip_no_prod', hostThisInstance, business)
    Wait(1000)
    TriggerEvent('esx_business:enableProp', 'counterfeit_upgrade_equip', hostThisInstance, business)
  end
  TriggerClientEvent('esx:showNotification', source, 'Estimated Time: 60 Mins')

  Wait(sleepTime)
  TriggerEvent('esx_business:enableProp', 'counterfeit_setup', hostThisInstance, business) -- this doesn't appear to be updating client props
end)


ESX.RegisterServerCallback('esx_business:canTakePrintedMoney', function(source, cb)
  local xPlayer = ESX.GetPlayerFromId(source)
  local money_wash_counterfeit_notes = xPlayer.getInventoryItem('money_wash_counterfeit_notes')

  if money_wash_counterfeit_notes.limit ~= -1 and (money_wash_counterfeit_notes.count + 1) > money_wash_counterfeit_notes.limit then
    TriggerClientEvent('esx:showNotification', source, "Inventory is Full")
    cb(false)
  else
    TriggerClientEvent('esx:showNotification', source, _U('action_start'))
    cb(true)
  end
end)

RegisterServerEvent('esx_business:takePrintedMoneyDone')
AddEventHandler('esx_business:takePrintedMoneyDone', function(business, hostThisInstance, amount)
  local xPlayer = ESX.GetPlayerFromId(source)
  local money_wash_counterfeit_notes = xPlayer.getInventoryItem('money_wash_counterfeit_notes')

  if money_wash_counterfeit_notes.limit ~= -1 and (money_wash_counterfeit_notes.count + 1) > money_wash_counterfeit_notes.limit then
    TriggerClientEvent('esx:showNotification', source, "Inventory is Full")
  else
    TriggerEvent('esx_business:disableProp', 'counterfeit_setup', hostThisInstance, business)

    if amount == Config.GiveCounterfeitNotesBasic then
      TriggerEvent('esx_business:disableProp', 'counterfeit_standard_equip', hostThisInstance, business)
      Wait(1000)
      TriggerEvent('esx_business:enableProp', 'counterfeit_standard_equip_no_prod', hostThisInstance, business)
    elseif amount == Config.GiveCounterfeitNotesUpgrade then
      TriggerEvent('esx_business:disableProp', 'counterfeit_upgrade_equip', hostThisInstance, business)
      Wait(1000)
      TriggerEvent('esx_business:enableProp', 'counterfeit_upgrade_equip_no_prod', hostThisInstance, business)
    end

    xPlayer.addInventoryItem('money_wash_counterfeit_notes', amount)
    TriggerClientEvent('esx:showNotification', source, _U('action_done'))
  end
end)


ESX.RegisterServerCallback('esx_business:canProduceDirtyMoney', function(source, cb)
  local xPlayer = ESX.GetPlayerFromId(source)
  local money_wash_counterfeit_notes = xPlayer.getInventoryItem('money_wash_counterfeit_notes')

  if money_wash_counterfeit_notes.count < 1 then
    TriggerClientEvent('esx:showNotification', source, _U('not_enough_items', 1, money_wash_counterfeit_notes.label))
    cb(false)
  else
    TriggerClientEvent('esx:showNotification', source, _U('action_start'))
    cb(true)
  end
end)

RegisterServerEvent('esx_business:produceDirtyMoneyDone')
AddEventHandler('esx_business:produceDirtyMoneyDone', function(business, hostThisInstance)
  local xPlayer = ESX.GetPlayerFromId(source)

  xPlayer.removeInventoryItem('money_wash_counterfeit_notes', 1)
  TriggerClientEvent('esx:showNotification', source, _U('action_done'))
  xPlayer.addAccountMoney('black_money', Config.GiveBlackMoneyAmount)
end)


ESX.RegisterServerCallback('esx_business:canWashMoney', function(source, cb, quantity)
  local xPlayer 		= ESX.GetPlayerFromId(source)
  local account 		= xPlayer.getAccount('black_money')

  if account.money < quantity then
    TriggerClientEvent('esx:showNotification', source, _U('not_enough_dirty'))
    cb(false)
  else
    TriggerClientEvent('esx:showNotification', source, _U('action_start'))
    cb(true)
  end
end)

RegisterServerEvent('esx_business:moneyWashDone')
AddEventHandler('esx_business:moneyWashDone', function(business, hostThisInstance, dryerVar, quantity)
  local xPlayer 		= ESX.GetPlayerFromId(source)
  xPlayer.removeAccountMoney('black_money', quantity)
  TriggerClientEvent('esx:showNotification', source, _U('action_done'))
  TriggerClientEvent('esx:showNotification', source, _U('wash_time', quantity/100))

  TriggerEvent('esx_business:disableProp', string.format("dryer%s_off", dryerVar), hostThisInstance, business)
  Wait(1000)
  TriggerEvent('esx_business:enableProp', string.format("dryer%s_on", dryerVar), hostThisInstance, business)
  Wait(1000 * (quantity/100)) --$1000 - 100s
  TriggerEvent('esx_business:disableProp', string.format("dryer%s_on", dryerVar), hostThisInstance, business)
  Wait(1000)
  TriggerEvent('esx_business:enableProp', string.format("dryer%s_open", dryerVar), hostThisInstance, business)

  local key = string.format("dryer%s_money", dryerVar)
  print("\nUPDATE owned_businesses SET " .. key .. " = " .. quantity .. " WHERE name = " .. business.name)
  MySQL.Sync.execute(
    'UPDATE owned_businesses SET ' .. key .. ' = @value WHERE name = @name',
    {
      ['@key']    = string.format("dryer%s_money", dryerVar),
      ['@value']  = quantity,
      ['@name']   = business.name
    }
  )
end)


RegisterServerEvent('esx_business:takeWashedMoneyDone')
AddEventHandler('esx_business:takeWashedMoneyDone', function(business, hostThisInstance, dryerVar)
  local xPlayer = ESX.GetPlayerFromId(source)

  TriggerClientEvent('esx:showNotification', source, _U('action_done'))
  TriggerEvent('esx_business:disableProp', string.format("dryer%s_open", dryerVar), hostThisInstance, business)
  Wait(1000)
  TriggerEvent('esx_business:enableProp', string.format("dryer%s_off", dryerVar), hostThisInstance, business)

  print("\nSELECT * FROM owned_businesses WHERE name = " .. business.name)
  MySQL.Async.fetchAll(
    'SELECT * FROM owned_businesses WHERE name = @name',
    {
      ['@name']  = business.name
    },
    function(ownedBusinesses)
      --print(json.encode(ownedBusinesses[1]))
      local key = string.format("dryer%s_money", dryerVar)
      local money = tonumber(ownedBusinesses[1][key])
      local moneyConverted = money * Config.BlackMoneyConversionFactor
      xPlayer.addMoney(moneyConverted)
    end
  )

  local key = string.format("dryer%s_money", dryerVar)
  print("\nUPDATE owned_businesses SET " .. key .. " = 0 WHERE name = " .. business.name)
  MySQL.Sync.execute(
    'UPDATE owned_businesses SET ' .. key .. ' = 0 WHERE name = @name',
    {
      ['@key']    = string.format("dryer%s_money", dryerVar),
      ['@name']   = business.name
    }
  )
end)
