local alarmTriggered = false

function math.randomchoice(t) --Selects a random item from a table
    local keys = {}
    for key, value in ipairs(t) do
        keys[#keys+1] = key --Store keys in another table
    end
    index = keys[math.random(1, #keys)]
    return t[index]
end

function getPoliceOnline()
  local xPlayers = ESX.GetPlayers()
  local cops = 0

  for i=1, #xPlayers, 1 do
    local xPlayer = ESX.GetPlayerFromId(xPlayers[i])
    if xPlayer.job.name == 'police' then
      cops = cops + 1
    end
  end

  return cops
end

ESX.RegisterServerCallback('esx_business:canStartChemicalHeist', function(source, cb)
  local xPlayer = ESX.GetPlayerFromId(source)

  --if getPoliceOnline() >= 2 then
    if xItem.limit ~= -1 and (xItem.count + 1) > xItem.limit then
      TriggerClientEvent('esx:showNotification', source, "Inventory is Full")
      cb(false)
    else
      TriggerClientEvent('esx:showNotification', source, _U('action_extract_start', xItem.label))
      TriggerClientEvent('esx_business:triggerAlarm', -1)
      alarmTriggered = true
      TriggerClientEvent('esx_business:setGuardsHostile', source)
      local xPlayers = ESX.GetPlayers()
      for i=1, #xPlayers, 1 do
  			local xPlayer = ESX.GetPlayerFromId(xPlayers[i])
  			if xPlayer.job.name == 'police' then
          local coords = {x=2661.61,y=1642.04,z=24.87}
  				TriggerClientEvent('esx:showNotification', xPlayers[i], _U('rob_in_prog', 'Robbery', 'Palmer-Taylor Power Station'))
  				TriggerClientEvent('esx_business:setblip', xPlayers[i], coords)

          local PlayerCoords = { x = coords.x, y = coords.y, z = coords.z }
          TriggerServerEvent('esx_addons_gcphone:startCall', 'police', 'Someone is stealing chemicals from the Power Station!', PlayerCoords, {PlayerCoords = { x = x, y = y, z = z }})
  			end
  		end
      cb(true)
    end
  --else
  --  TriggerClientEvent('esx:showNotification', source, _U('not_enough_cops'))
  --  cb(false)
  --end
end)


RegisterServerEvent('esx_business:chemicalHeistDone')
AddEventHandler('esx_business:chemicalHeistDone', function()
  local xPlayer = ESX.GetPlayerFromId(source)
  local chemicals = {'acetone','anhydrous_ammonia','hydrochloric_acid','lithium','red_phosphorus','toluene','sodium_hydroxide','sulfuric_acid'}
  local randomItem = math.randomchoice(chemicals)
  local xItem = xPlayer.getInventoryItem(randomItem)

  xPlayer.addInventoryItem(randomItem, 1)
  local xPlayers = ESX.GetPlayers()
  for i=1, #xPlayers, 1 do
    local xPlayer = ESX.GetPlayerFromId(xPlayers[i])
    if xPlayer.job.name == 'police' then
      local coords = {x=2661.61,y=1642.04,z=24.87}
      TriggerClientEvent('esx_business:killblip', xPlayers[i])
    end
  end
  TriggerClientEvent('esx:showNotification', source, _U('action_extract_done', xItem.label))
end)

RegisterServerEvent('esx_business:disableAlarm')
AddEventHandler('esx_business:disableAlarm', function()
	local xPlayers = ESX.GetPlayers()

	if not alarmTriggered then
		TriggerClientEvent('esx:showNotification', source, 'The alarm is already disabled')
	else
		TriggerClientEvent('esx:showNotification', source, 'The alarm has been disabled')
		TriggerClientEvent('esx_business:killAlarm', -1)
	end
end)


ESX.RegisterServerCallback('esx_business:canProduceMeth', function(source, cb)
  local xPlayer = ESX.GetPlayerFromId(source)
  local unprocessed_meth_good = xPlayer.getInventoryItem('unprocessed_meth_good')
  local acetone = xPlayer.getInventoryItem('acetone')
  local anhydrous_ammonia = xPlayer.getInventoryItem('anhydrous_ammonia')
  local pseudoephedrine = xPlayer.getInventoryItem('pseudoephedrine')
  local hydrochloric_acid = xPlayer.getInventoryItem('hydrochloric_acid')
  local lithium = xPlayer.getInventoryItem('lithium')
  local red_phosphorus = xPlayer.getInventoryItem('red_phosphorus')
  local toluene = xPlayer.getInventoryItem('toluene')
  local sodium_hydroxide = xPlayer.getInventoryItem('sodium_hydroxide')
  local sulfuric_acid = xPlayer.getInventoryItem('sulfuric_acid')

  if unprocessed_meth_good.limit ~= -1 and (unprocessed_meth_good.count + 1) > unprocessed_meth_good.limit then
    TriggerClientEvent('esx:showNotification', source, "Inventory is Full")
    cb(false)
  elseif acetone.count < 1 then
    TriggerClientEvent('esx:showNotification', source, _U('not_enough_items', 1, acetone.label))
    cb(false)
  elseif anhydrous_ammonia.count < 1 then
    TriggerClientEvent('esx:showNotification', source, _U('not_enough_items', 1, anhydrous_ammonia.label))
    cb(false)
  elseif pseudoephedrine.count < 1 then
    TriggerClientEvent('esx:showNotification', source, _U('not_enough_items', 1, pseudoephedrine.label))
    cb(false)
  elseif hydrochloric_acid.count < 1 then
    TriggerClientEvent('esx:showNotification', source, _U('not_enough_items', 1, hydrochloric_acid.label))
    cb(false)
  elseif lithium.count < 1 then
    TriggerClientEvent('esx:showNotification', source, _U('not_enough_items', 1, lithium.label))
    cb(false)
  elseif red_phosphorus.count < 1 then
    TriggerClientEvent('esx:showNotification', source, _U('not_enough_items', 1, red_phosphorus.label))
    cb(false)
  elseif toluene.count < 1 then
    TriggerClientEvent('esx:showNotification', source, _U('not_enough_items', 1, toluene.label))
    cb(false)
  elseif sodium_hydroxide.count < 1 then
    TriggerClientEvent('esx:showNotification', source, _U('not_enough_items', 1, sodium_hydroxide.label))
    cb(false)
  elseif sulfuric_acid.count < 1 then
    TriggerClientEvent('esx:showNotification', source, _U('not_enough_items', 1, sulfuric_acid.label))
    cb(false)
  else
    TriggerClientEvent('esx:showNotification', source, _U('action_start'))
    cb(true)
  end
end)

RegisterServerEvent('esx_business:produceMethDone')
AddEventHandler('esx_business:produceMethDone', function(business, hostThisInstance, sleepTime)
  local xPlayer = ESX.GetPlayerFromId(source)

  --maybe we should block inventory key while doing actions, aye?
  -- they may have dropped their items to dupe them during action time

  xPlayer.removeInventoryItem('acetone', 1)
  xPlayer.removeInventoryItem('anhydrous_ammonia', 1)
  xPlayer.removeInventoryItem('pseudoephedrine', 1)
  xPlayer.removeInventoryItem('hydrochloric_acid', 1)
  xPlayer.removeInventoryItem('lithium', 1)
  xPlayer.removeInventoryItem('red_phosphorus', 1)
  xPlayer.removeInventoryItem('toluene', 1)
  xPlayer.removeInventoryItem('sodium_hydroxide', 1)
  xPlayer.removeInventoryItem('sulfuric_acid', 1)

  Citizen.Wait(sleepTime)
  TriggerEvent('esx_business:putItemServer', xPlayer.identifier, 'item_standard', 'unprocessed_meth_good', 1)
end)


ESX.RegisterServerCallback('esx_business:canProcessMeth', function(source, cb)
  local xPlayer = ESX.GetPlayerFromId(source)
  local unprocessed_meth_good = xPlayer.getInventoryItem('unprocessed_meth_good')
  local processed_meth = xPlayer.getInventoryItem('processed_meth')

  if processed_meth.limit ~= -1 and (processed_meth.count + 1) > processed_meth.limit then
    TriggerClientEvent('esx:showNotification', source, "Inventory is Full")
    cb(false)
  elseif unprocessed_meth_good.count < 1 then --anti exploit checks
    TriggerClientEvent('esx:showNotification', source, _U('not_enough_items', 1, unprocessed_meth_good.label))
    cb(false)
  else
    TriggerClientEvent('esx:showNotification', source, _U('action_start'))
    cb(true)
  end
end)

RegisterServerEvent('esx_business:processMethDone')
AddEventHandler('esx_business:processMethDone', function(business, hostThisInstance, amount)
  local xPlayer = ESX.GetPlayerFromId(source)
  local processed_meth = xPlayer.getInventoryItem('processed_meth')
  local unprocessed_meth_good = xPlayer.getInventoryItem('unprocessed_meth_good')

  --maybe we should block inventory key while doing actions, aye?
  -- they may have dropped their items to dupe them during action time

  xPlayer.removeInventoryItem('unprocessed_meth_good', unprocessed_meth_good.count)
  xPlayer.addInventoryItem('processed_meth', unprocessed_meth_good.count * amount)
  TriggerClientEvent('esx:showNotification', source, _U('action_done'))
end)
