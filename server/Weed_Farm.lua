ESX.RegisterServerCallback('esx_business:canGrowWeed', function(source, cb)
  local xPlayer = ESX.GetPlayerFromId(source) --attempt to index nil vlalue

  local weed_seed = xPlayer.getInventoryItem('weed_seed')
  local weed_seed_count = weed_seed.count
  local weed_planter = xPlayer.getInventoryItem('weed_planter')
  local weed_planter_count = weed_planter.count

  if weed_seed_count < 9 then
    TriggerClientEvent('esx:showNotification', source, _U('not_enough_items', 9, weed_seed.label))
    cb(false)
  elseif weed_planter_count < 9 then
    TriggerClientEvent('esx:showNotification', source, _U('not_enough_items', 9, weed_planter.label))
    cb(false)
  else
    TriggerClientEvent('esx:showNotification', source, _U('action_start'))
    cb(true)
  end
end)

RegisterServerEvent('esx_business:weedGrowDone')
AddEventHandler('esx_business:weedGrowDone', function(business, hostThisInstance, growVar, sleepTime)
  local xPlayer = ESX.GetPlayerFromId(source) --attempt to index nil vlalue

  local weed_seed = xPlayer.getInventoryItem('weed_seed')
  local weed_seed_count = weed_seed.count
  local weed_planter = xPlayer.getInventoryItem('weed_planter')
  local weed_planter_count = weed_planter.count

  if weed_seed_count < 9 then --check to see if they dropped their items during the action
    TriggerClientEvent('esx:showNotification', source, _U('not_enough_items', 9, weed_seed.label))
  elseif weed_planter_count < 9 then --check to see if they dropped their items during the action
    TriggerClientEvent('esx:showNotification', source, _U('not_enough_items', 9, weed_planter.label))
  else
    xPlayer.removeInventoryItem('weed_seed', 9)
    xPlayer.removeInventoryItem('weed_planter', 9)
    TriggerClientEvent('esx:showNotification', source, _U('action_done'))
    TriggerEvent('esx_business:enableProp', string.format("weed_growth%s_stage1", growVar), hostThisInstance, business)
    Wait(sleepTime)
    TriggerEvent('esx_business:enableProp', string.format("weed_growth%s_stage2", growVar), hostThisInstance, business)
    TriggerEvent('esx_business:disableProp', string.format("weed_growth%s_stage1", growVar), hostThisInstance, business)
    Wait(sleepTime)
    TriggerEvent('esx_business:enableProp', string.format("weed_growth%s_stage3", growVar), hostThisInstance, business)
    TriggerEvent('esx_business:disableProp', string.format("weed_growth%s_stage2", growVar), hostThisInstance, business)
  end
end)


ESX.RegisterServerCallback('esx_business:canHarvestWeed', function(source, cb)
  local xPlayer = ESX.GetPlayerFromId(source)
  local weed_undried = xPlayer.getInventoryItem('weed_undried')
  local weed_seed = xPlayer.getInventoryItem('weed_seed')

  if weed_undried.limit ~= -1 and (weed_undried.count + 1) > weed_undried.limit then
    TriggerClientEvent('esx:showNotification', source, "Inventory is Full")
    cb(false)
  elseif weed_seed.limit ~= -1 and (weed_seed.count + 1) > weed_seed.limit then
    TriggerClientEvent('esx:showNotification', source, "Inventory is Full")
    cb(false)
  else
    TriggerClientEvent('esx:showNotification', source, _U('action_start'))
    cb(true)
  end
end)

RegisterServerEvent('esx_business:harvestWeedDone')
AddEventHandler('esx_business:harvestWeedDone', function(business, hostThisInstance, growVar, amount)
  local xPlayer = ESX.GetPlayerFromId(source)
  local weed_undried = xPlayer.getInventoryItem('weed_undried')

  TriggerEvent('esx_business:disableProp', string.format("weed_growth%s_stage3", growVar), hostThisInstance, business)
  xPlayer.addInventoryItem('weed_undried', amount)
  xPlayer.addInventoryItem('weed_seed', 9)
  xPlayer.addInventoryItem('weed_planter', 9)
  TriggerClientEvent('esx:showNotification', source, _U('action_done'))
end)


ESX.RegisterServerCallback('esx_business:canHangWeed', function(source, cb)
  local xPlayer = ESX.GetPlayerFromId(source)
  local weed_dried = xPlayer.getInventoryItem('weed_dried')
  local weed_undried = xPlayer.getInventoryItem('weed_undried')
  local weed_undried_count = weed_undried.count

  if weed_undried_count < 1 then
    TriggerClientEvent('esx:showNotification', source, _U('not_enough_items', 1, weed_undried.label))
    cb(false)
  else
    TriggerClientEvent('esx:showNotification', source, _U('action_start'))
    cb(true)
  end
end)

RegisterServerEvent('esx_business:hangWeedDone')
AddEventHandler('esx_business:hangWeedDone', function(business, hostThisInstance)
  local xPlayer = ESX.GetPlayerFromId(source)
  local weed_dried = xPlayer.getInventoryItem('weed_dried')
  local weed_undried = xPlayer.getInventoryItem('weed_undried')
  local weed_undried_count = weed_undried.count

  if weed_dried.limit ~= -1 and (weed_dried.count + weed_undried_count) > weed_dried.limit then
    TriggerClientEvent('esx:showNotification', source, "Inventory is Full")
  elseif weed_undried_count < 1 then--check to see if they dropped their items during the action
    TriggerClientEvent('esx:showNotification', source, _U('not_enough_items', weed_undried_count, weed_undried.label))
  else
    TriggerEvent('esx_business:enableProp', 'weed_drying', hostThisInstance, business)
    xPlayer.removeInventoryItem('weed_undried', weed_undried_count)
    Citizen.Wait((1000 * 60) * Config.TimeToDryWeed)
    TriggerEvent('esx_business:disableProp', 'weed_drying', hostThisInstance, business)
    TriggerEvent('esx_business:putItemServer', xPlayer.identifier, 'item_standard', 'weed_dried', weed_undried_count)
  end
end)


ESX.RegisterServerCallback('esx_business:canProcessWeed', function(source, cb)
  local xPlayer = ESX.GetPlayerFromId(source)
  local weed_dried = xPlayer.getInventoryItem('weed_dried')

  if weed_dried.count < 9 then
    TriggerClientEvent('esx:showNotification', source, _U('not_enough_items', 9, weed_dried.label))
    cb(false)
  else
    TriggerClientEvent('esx:showNotification', source, _U('action_start'))
    cb(true)
  end
end)

RegisterServerEvent('esx_business:processWeedDone')
AddEventHandler('esx_business:processWeedDone', function(business, hostThisInstance)
  local xPlayer = ESX.GetPlayerFromId(source)
  local weed_dried = xPlayer.getInventoryItem('weed_dried')
  local xItem = xPlayer.getInventoryItem('weed_pooch')

  if xItem.limit ~= -1 and (xItem.count + 9) > xItem.limit then
    TriggerClientEvent('esx:showNotification', source, "Inventory is Full")
  else
    xPlayer.removeInventoryItem('weed_dried', 9)
    xPlayer.addInventoryItem('processed_weed', 27)
    TriggerClientEvent('esx:showNotification', source, _U('action_done'))
  end
end)
