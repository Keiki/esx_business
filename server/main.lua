ESX = nil
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

function GetBusiness(name)
  for i=1, #Config.Businesses, 1 do
    if Config.Businesses[i].name == name then
      return Config.Businesses[i]
    end
  end
end

function SetBusinessOwned(name, label, type, price, owner)
  MySQL.Async.execute(
    'INSERT INTO owned_businesses (name, label, type, owner) VALUES (@name, @label, @type, @owner)',
    {
      ['@name']   = name,
      ['@label']  = label,
      ['@type']  = type,
      ['@owner']  = owner
    }, function(rowsChanged)
		local xPlayer = ESX.GetPlayerFromIdentifier(owner)

		if xPlayer then
			TriggerClientEvent('esx_business:setBusinessOwned', xPlayer.source, name, true)
      TriggerClientEvent('esx:showNotification', xPlayer.source, _U('purchased_for', price))
		end
	end)

  MySQL.Async.execute(
    'UPDATE businesses SET is_owned = @is_owned WHERE name = @name',
    {
      ['@is_owned'] = 1,
      ['@name']    = name
    }
  )
end

function RemoveOwnedBusiness(name, owner)
  MySQL.Async.execute(
    'DELETE FROM owned_businesses WHERE name = @name',
    {
      ['@name']  = name
    },
    function()
      local xPlayers = ESX.GetPlayers()
      local xPlayer = ESX.GetPlayerFromId(xPlayers[i])

      for i=1, #xPlayers, 1 do
        local xPlayer = ESX.GetPlayerFromId(xPlayers[i])

        if xPlayer.identifier == owner then
          TriggerClientEvent('esx_business:setBusinessOwned', xPlayer.source, name, false)
          TriggerClientEvent('esx:showNotification', xPlayer.source, _U('sold_business'))
          break
        end
      end
    end
  )

  MySQL.Async.execute(
    'UPDATE businesses SET is_owned = @is_owned WHERE name = @name',
    {
      ['@is_owned'] = 0,
      ['@name']    = name
    }
  )
end



AddEventHandler('onMySQLReady', function ()
  MySQL.Async.fetchAll('SELECT * FROM businesses', {}, function(businesses)

    for i=1, #businesses, 1 do
      local entering      = nil
      local outside       = nil
      local isOwned       = nil

      if businesses[i].entering ~= nil then
        entering = json.decode(businesses[i].entering)
      end

      if businesses[i].outside ~= nil then
        outside = json.decode(businesses[i].outside)
      end

      if businesses[i].is_owned == 0 then
        isOwned = false
      else
        isOwned = true
      end

      table.insert(Config.Businesses, {
        name          = businesses[i].name,
        entering      = entering,
        outside       = outside,
        isOwned       = isOwned
      })
    end

    TriggerClientEvent('esx_business:sendBusinesses', -1, Config.Businesses)
  end)
end)


AddEventHandler('esx_business:setBusinessOwned', function(name, type, price, owner)
  SetBusinessOwned(name, type, price, owner)
end)


AddEventHandler('esx_business:removeOwnedBusiness', function(name, owner)
  RemoveOwnedBusiness(name, owner)
end)


RegisterServerEvent('esx_business:RefreshBlips')
AddEventHandler('esx_business:RefreshBlips', function()
  TriggerClientEvent('esx_business:RefreshBlips', -1)
end)

RegisterServerEvent('esx_business:buyBusiness')
AddEventHandler('esx_business:buyBusiness', function(businessName, label, type, price)
  local xPlayer  = ESX.GetPlayerFromId(source)
  local business = GetBusiness(businessName)
  local accountMoney = xPlayer.getAccount('bank').money

  if price <= accountMoney then
    xPlayer.removeAccountMoney('bank', price)
    SetBusinessOwned(businessName, label, type, price, xPlayer.identifier)
  else
    TriggerClientEvent('esx:showNotification', source, _U('not_enough'))
  end
end)


RegisterServerEvent('esx_business:removeOwnedBusiness')
AddEventHandler('esx_business:removeOwnedBusiness', function(businessName, price)
  local xPlayer = ESX.GetPlayerFromId(source)
  xPlayer.addAccountMoney('bank', price)
  RemoveOwnedBusiness(businessName, xPlayer.identifier)
end)


AddEventHandler('esx_business:removeOwnedBusinessIdentifier', function(businessName, identifier)
  RemoveOwnedBusiness(businessName, identifier)
end)


RegisterServerEvent('esx_business:saveLastBusiness')
AddEventHandler('esx_business:saveLastBusiness', function(business)
  local xPlayer = ESX.GetPlayerFromId(source)

  MySQL.Async.execute(
    'UPDATE users SET last_business = @last_business WHERE identifier = @identifier',
    {
      ['@last_business'] = business,
      ['@identifier']    = xPlayer.identifier
    }
  )
end)


RegisterServerEvent('esx_business:deleteLastBusiness')
AddEventHandler('esx_business:deleteLastBusiness', function()
  local xPlayer = ESX.GetPlayerFromId(source)

  MySQL.Async.execute(
    'UPDATE users SET last_business = NULL WHERE identifier = @identifier',
    {
      ['@identifier']    = xPlayer.identifier
    }
  )
end)


RegisterServerEvent('esx_business:getItem')
AddEventHandler('esx_business:getItem', function(name, owner, type, item, count)
	local _source      = source
	local xPlayer      = ESX.GetPlayerFromId(_source)
	local xPlayerOwner = ESX.GetPlayerFromIdentifier(owner)

	if type == 'item_standard' then

		local sourceItem = xPlayer.getInventoryItem(item)

		TriggerEvent('esx_addoninventory:getInventory', 'business', xPlayerOwner.identifier, function(inventory)
			local inventoryItem = inventory.getItem(item)

			-- is there enough in the business?
			if count > 0 and inventoryItem.count >= count then

				-- can the player carry the said amount of x item?
				if sourceItem.limit ~= -1 and (sourceItem.count + count) > sourceItem.limit then
					TriggerClientEvent('esx:showNotification', _source, _U('player_cannot_hold'))
				else
					inventory.removeItem(item, count)
					xPlayer.addInventoryItem(item, count)
					TriggerClientEvent('esx:showNotification', _source, _U('have_withdrawn', count, inventoryItem.label))
				end
			else
				TriggerClientEvent('esx:showNotification', _source, _U('not_enough_in_business'))
			end
		end)

	elseif type == 'item_account' then

		TriggerEvent('esx_addonaccount:getAccount', 'business_' .. item, xPlayerOwner.identifier, function(account)
			local roomAccountMoney = account.money

			if roomAccountMoney >= count then
				account.removeMoney(count)
				xPlayer.addAccountMoney(item, count)
			else
				TriggerClientEvent('esx:showNotification', _source, _U('amount_invalid'))
			end
		end)

	elseif type == 'item_weapon' then

		TriggerEvent('esx_datastore:getDataStore', 'business', xPlayerOwner.identifier, function(store)
			local storeWeapons = store.get('weapons') or {}
			local weaponName   = nil
			local ammo         = nil

			for i=1, #storeWeapons, 1 do
				if storeWeapons[i].name == item then
					weaponName = storeWeapons[i].name
					ammo       = storeWeapons[i].ammo

					table.remove(storeWeapons, i)
					break
				end
			end

			store.set('weapons', storeWeapons)
			xPlayer.addWeapon(weaponName, ammo)
		end)

	end
end)

RegisterServerEvent('esx_business:putItem')
AddEventHandler('esx_business:putItem', function(name, owner, type, item, count)
	local _source      = source
	local xPlayer      = ESX.GetPlayerFromId(_source)
	local xPlayerOwner = ESX.GetPlayerFromIdentifier(owner)

	if type == 'item_standard' then

		local playerItemCount = xPlayer.getInventoryItem(item).count

		if playerItemCount >= count and count > 0 then
      TriggerEvent('esx_addoninventory:getInventory', 'business', xPlayerOwner.identifier, function(inventory)
			--TriggerEvent('esx_addoninventory:getInventory', 'business_' .. name, xPlayerOwner.identifier, function(inventory) NIL FIELD WHY
				xPlayer.removeInventoryItem(item, count)
				inventory.addItem(item, count)
				TriggerClientEvent('esx:showNotification', _source, _U('have_deposited', count, inventory.getItem(item).label))
			end)
		else
			TriggerClientEvent('esx:showNotification', _source, _U('invalid_quantity'))
		end

	elseif type == 'item_account' then

		local playerAccountMoney = xPlayer.getAccount(item).money

		if playerAccountMoney >= count and count > 0 then
			xPlayer.removeAccountMoney(item, count)

			TriggerEvent('esx_addonaccount:getAccount', 'business_' .. item, xPlayerOwner.identifier, function(account)
				account.addMoney(count)
			end)
		else
			TriggerClientEvent('esx:showNotification', _source, _U('amount_invalid'))
		end

	elseif type == 'item_weapon' then

		TriggerEvent('esx_datastore:getDataStore', 'business', xPlayerOwner.identifier, function(store)
			local storeWeapons = store.get('weapons') or {}

			table.insert(storeWeapons, {
				name = item,
				ammo = count
			})

			store.set('weapons', storeWeapons)
			xPlayer.removeWeapon(item)
		end)

	end
end)

RegisterServerEvent('esx_business:putItemServer')
AddEventHandler('esx_business:putItemServer', function(owner, type, item, count)
	if type == 'item_standard' then
		TriggerEvent('esx_addoninventory:getInventory', 'business', owner, function(inventory)
			print("owner: " .. owner)
			print("item: " .. item)
			print("count: " .. count)
			inventory.addItem(item, count)
		end)
	elseif type == 'item_account' then
		TriggerEvent('esx_addonaccount:getAccount', 'business_' .. item, owner, function(account)
			account.addMoney(count)
		end)
	elseif type == 'item_weapon' then
		TriggerEvent('esx_datastore:getDataStore', 'business', owner, function(store)
			local storeWeapons = store.get('weapons') or {}

			table.insert(storeWeapons, {
				name = item,
				ammo = count
			})

			store.set('weapons', storeWeapons)
		end)
	end
end)


RegisterServerEvent('esx_business:enableProp')
AddEventHandler('esx_business:enableProp', function(prop, hostThisInstance, business)
	local business = business

	TriggerClientEvent('esx_business:enableProp', -1, prop, hostThisInstance)
	local props_table = {}
	print("\nSELECT FROM owned_businesses WHERE name = " .. business.name)
	MySQL.Async.fetchAll(
	'SELECT * FROM owned_businesses WHERE name = @name',
	{
	  ['@name']  = business.name
	},
	function(ownedBusinesses)
	  props_table = json.decode(ownedBusinesses[1].props)
	  print("\nprops_table before: " .. json.encode(props_table))
	  table.insert(props_table, prop)
	  print("attempt to insert: " .. prop)
	  print("\nprops_table after: " .. json.encode(props_table))
	  --TriggerEvent('esx_business:updateProps', business, props_table)
    updateProps(business, props_table)
	end
	)

  --Wait(1000)
end)


RegisterServerEvent('esx_business:disableProp')
AddEventHandler('esx_business:disableProp', function(prop, hostThisInstance, business)
	if business == nil then --why tho
    return
  end

	TriggerClientEvent('esx_business:disableProp', -1, prop, hostThisInstance)
	local props_table = {}
	print("\nSELECT FROM owned_businesses WHERE name = " .. business.name) -- this is nil
	MySQL.Async.fetchAll(
	'SELECT * FROM owned_businesses WHERE name = @name',
	{
	  ['@name']  = business.name -- this is nil
	},
	function(ownedBusinesses)
	  props_table = json.decode(ownedBusinesses[1].props)
	  print("\nprops_table before: " .. json.encode(props_table))
	  for k,v in pairs(props_table) do
  		--print("k: " .. k .. " v: " .. v)
  		if v == prop then
  		  table.remove(props_table,k)
  		  print("attempt to remove: " .. v)
  		  print("\nprops_table after: " .. json.encode(props_table))
  		  --TriggerEvent('esx_business:updateProps', business, props_table)
        updateProps(business, props_table)
  		end
	  end
	end
	)

  --Wait(1000)
end)

RegisterServerEvent('esx_business:installEquipment')
AddEventHandler('esx_business:installEquipment', function(item, host)
  local xPlayer = ESX.GetPlayerFromId(source)
  local item = xPlayer.getInventoryItem(item)
  xPlayer.removeInventoryItem(item.name, 1) --might need to be propName
  TriggerClientEvent('esx:showNotification', source, _U('installed_equipment', item.label))
end)


function MySQLAsyncExecute(query)
    local IsBusy = true
    local result = nil
    MySQL.Async.fetchAll(query, {}, function(data)
        result = data
        IsBusy = false
    end)
    while IsBusy do
        Citizen.Wait(0)
    end
    return result
end


function updateProps(business, props_table)
  MySQLAsyncExecute("UPDATE `owned_businesses` SET `props` = '"..json.encode(props_table).."' WHERE `name` = '"..business.name.."'")
end

ESX.RegisterServerCallback('esx_business:getProps', function(source, cb, name, hostThisInstance)
  MySQL.Async.fetchAll(
    'SELECT * FROM owned_businesses WHERE name = @name',
    {
      ['@name']  = name
    },
    function(ownedBusinesses)
      cb(json.decode(ownedBusinesses[1].props), hostThisInstance)
      --print("hostThisInstance: " .. hostThisInstance)
    end
  )
end)


ESX.RegisterServerCallback('esx_business:getBusinessType', function(source, cb, name)
  MySQL.Async.fetchAll(
    'SELECT * FROM owned_businesses WHERE name = @name',
    {
      ['@name'] = name
    },
    function(ownedBusinesses)
      cb(ownedBusinesses[1].type)
    end
  )
end)



ESX.RegisterServerCallback('esx_business:getBusinessLabel', function(source, cb, name)
  MySQL.Async.fetchAll(
    'SELECT * FROM owned_businesses WHERE name = @name',
    {
      ['@name'] = name
    },
    function(ownedBusinesses)
      cb(ownedBusinesses[1].label)
    end
  )
end)


ESX.RegisterServerCallback('esx_business:getBusinessIsOwned', function(source, cb, name)
  MySQL.Async.fetchAll(
    'SELECT * FROM businesses WHERE name = @name',
    {
      ['@name'] = name
    },
    function(businesses)
      cb(businesses[1].is_owned)
    end
  )
end)


ESX.RegisterServerCallback('esx_business:getBusinesses', function(source, cb)
  cb(Config.Businesses)
end)

function table.contains(table, element)
  for _, value in pairs(table) do
    if value == element then
      return true
    end
  end
  return false
end

ESX.RegisterServerCallback('esx_business:getOwnedBusinesses', function(source, cb)
	local xPlayer = ESX.GetPlayerFromId(source)

	MySQL.Async.fetchAll('SELECT * FROM owned_businesses WHERE owner = @owner', {
	['@owner'] = xPlayer.identifier
  }, function(ownedBusinesses)
		local businesses = {}

		for i=1, #ownedBusinesses, 1 do
			table.insert(businesses, ownedBusinesses[i].name)
		end

		cb(businesses)
	end)
end)


ESX.RegisterServerCallback('esx_business:getLastBusiness', function(source, cb)
  local xPlayer = ESX.GetPlayerFromId(source)

  MySQL.Async.fetchAll(
    'SELECT * FROM users WHERE identifier = @identifier',
    {
      ['@identifier'] = xPlayer.identifier
    },
    function(users)
      cb(users[1].last_business)
    end
  )
end)


ESX.RegisterServerCallback('esx_business:getBusinessInventory', function(source, cb, owner)
	local xPlayer    = ESX.GetPlayerFromIdentifier(owner)
	local blackMoney = 0
	local items      = {}
	local weapons    = {}

	TriggerEvent('esx_addonaccount:getAccount', 'business_black_money', xPlayer.identifier, function(account)
		blackMoney = account.money
	end)

	TriggerEvent('esx_addoninventory:getInventory', 'business', xPlayer.identifier, function(inventory)
		items = inventory.items
	end)

	TriggerEvent('esx_datastore:getDataStore', 'business', xPlayer.identifier, function(store)
		local storeWeapons = store.get('weapons') or {}
	end)

	cb({
		blackMoney = blackMoney,
		items      = items,
		weapons    = weapons
	})
end)


ESX.RegisterServerCallback('esx_business:getPlayerInventory', function(source, cb)
	local xPlayer    = ESX.GetPlayerFromId(source)
	local blackMoney = xPlayer.getAccount('black_money').money
	local items      = xPlayer.inventory

	cb({
		blackMoney = blackMoney,
		items      = items
	})
end)


--[[
ESX.RegisterServerCallback('esx_business:getPlayerDressing', function(source, cb)
	local xPlayer  = ESX.GetPlayerFromId(source)

	TriggerEvent('esx_datastore:getDataStore', 'business', xPlayer.identifier, function(store)
		local count    = store.count('dressing')
		local labels   = {}

		for i=1, count, 1 do
			local entry = store.get('dressing', i)
			table.insert(labels, entry.label)
		end

		cb(labels)
	end)
end)

ESX.RegisterServerCallback('esx_business:getPlayerOutfit', function(source, cb, num)
	local xPlayer  = ESX.GetPlayerFromId(source)

	TriggerEvent('esx_datastore:getDataStore', 'business', xPlayer.identifier, function(store)
		local outfit = store.get('dressing', num)
		cb(outfit.skin)
	end)
end)

RegisterServerEvent('esx_business:removeOutfit')
AddEventHandler('esx_business:removeOutfit', function(label)
	local xPlayer = ESX.GetPlayerFromId(source)

	TriggerEvent('esx_datastore:getDataStore', 'business', xPlayer.identifier, function(store)
		local dressing = store.get('dressing') or {}

		table.remove(dressing, label)
		store.set('dressing', dressing)
	end)
end)
]]
