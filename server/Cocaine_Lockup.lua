ESX = nil
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)


ESX.RegisterServerCallback('esx_business:canProcessCoke', function(source, cb)
  print("\ncokeNumber: " ..cokeNumber)
  local xPlayer = ESX.GetPlayerFromId(source)
  local unprocessed_cocaine = xPlayer.getInventoryItem('unprocessed_cocaine')
  local coke_pooch = xPlayer.getInventoryItem('coke_pooch')

  if unprocessed_cocaine.count < 1 then
    TriggerClientEvent('esx:showNotification', source, _U('not_enough_items', 1, unprocessed_cocaine.label))
    cb(false)
  elseif coke_pooch.limit ~= -1 and (coke_pooch.count + 1) > coke_pooch.limit then
    TriggerClientEvent('esx:showNotification', source, "Inventory is Full")
    cb(false)
  else
    TriggerClientEvent('esx:showNotification', source, _U('action_start'))
    cb(true)
  end
end)

RegisterServerEvent('esx_business:processCokeDone')
AddEventHandler('esx_business:processCokeDone', function(business, hostThisInstance)
  local xPlayer = ESX.GetPlayerFromId(source)
  local unprocessed_cocaine = xPlayer.getInventoryItem('unprocessed_cocaine')
  local xItem = xPlayer.getInventoryItem('coke_pooch')

  if xItem.limit ~= -1 and (xItem.count + 1) > xItem.limit then
    TriggerClientEvent('esx:showNotification', source, "Inventory is Full")
  elseif unprocessed_cocaine.count < 1 then--check to see if they dropped their items during the action
    TriggerClientEvent('esx:showNotification', source, _U('not_enough_items', 1, unprocessed_cocaine.label))
  else
    xPlayer.removeInventoryItem('unprocessed_cocaine', 1)
    xPlayer.addInventoryItem('coke_pooch', 10)
    TriggerClientEvent('esx:showNotification', source, _U('action_done'))
  end
end)
