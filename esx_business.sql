USE `essentialmode`;

ALTER TABLE `users`
  ADD COLUMN `last_business` VARCHAR(255) NULL
;

INSERT INTO `addon_account` (name, label, shared) VALUES
  ('business_black_money','Dirty Money',0)
;

INSERT INTO `addon_inventory` (name, label, shared) VALUES
  ('business','Business',0)
;

INSERT INTO `datastore` (name, label, shared) VALUES
  ('business','Business',0)
;

CREATE TABLE `owned_businesses` (

  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `label` varchar(255) NOT NULL,
  `type` varchar(45) DEFAULT NULL,
  `owner` varchar(60) NOT NULL,
  `props` varchar(255) DEFAULT '[]',

  PRIMARY KEY (`id`)
);

CREATE TABLE `businesses` (

  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `entering` varchar(255) DEFAULT NULL,
  `outside` varchar(255) DEFAULT NULL,
  `is_owned` int(11) DEFAULT NULL,

  PRIMARY KEY (`id`)
);

INSERT INTO `shops` (store, item, price) VALUES
	('BlackMarket','weed_seed',100),
	('BlackMarket','weed_planter',1000),
	('BlackMarket','weed_equip_basic',10000),
	('BlackMarket','weed_equip_upgrade',20000),
	('BlackMarket','coke_equip_basic',50000),
	('BlackMarket','coke_equip_upgrade',50000),
	('BlackMarket','meth_equip_basic',1000000),
	('BlackMarket','meth_equip_upgrade',2000000),
	('BlackMarket','money_wash_equip_basic',500000),
	('BlackMarket','money_wash_equip_upgrade',1000000),
	('BlackMarket','money_wash_dryera',50000),
	('BlackMarket','money_wash_dryerb',50000),
	('BlackMarket','money_wash_dryerc',50000),
	('BlackMarket','money_wash_dryerd',50000),
	('BlackMarket','money_wash_plates',1000000),
	('BlackMarket','money_wash_blank_notes',500),
	('BlackMarket','doc_forge_supplies',10000),
	('BlackMarket','doc_forge_equip_basic',10000),
	('BlackMarket','doc_forge_equip_upgrade',20000),
	('BlackMarket','doc_forge_interior_upgrade',20000),
	('BlackMarket','pseudoephedrine',10000),
	('BlackMarket','red_phosphorus',10000),
	('BlackMarket','sodium_hydroxide',10000),
	('BlackMarket','sulfuric_acid',10000),
	('BlackMarket','toluene',10000),
	('BlackMarket','lithium',10000),
	('BlackMarket','hydrochloric_acid',10000),
	('BlackMarket','anhydrous_ammonia',10000),
	('BlackMarket','acetone',10000)
;

INSERT INTO `items` (`name`, `label`, `limit`) VALUES
	('weed_equip_basic', 'Basic Grow Equipment', 1),
	('weed_equip_upgrade', 'Upgraded Grow Equipment', 1),
	('weed_dried', 'Dried Marijuana', 90),
	('weed_planter', 'Marijuana Planter', 9),
	('weed_seed', 'Marijuana Seed', 9),
	('weed_undried', 'Undried Marijuana', 90),
	('coke_equip_basic', 'Basic Cocaine Equipment', 1),
	('coke_equip_upgrade', 'Upgraded Cocaine Equipment', 1),
	('unprocessed_cocaine', 'Unprocessed Cocaine', 1),
	('meth_equip_basic', 'Basic Meth Equipment', 1),
	('meth_equip_upgrade', 'Upgraded Meth Equipment', 1),
	('unprocessed_meth', 'Unprocessed Methamphetamine', 10),
	('money_wash_equip_basic', 'Basic Laundry Equipment', 1),
	('money_wash_equip_upgrade', 'Upgraded Laundry Equipment', 1),
	('money_wash_blank_notes', 'Blank Counterfeit Notes', 50),
	('money_wash_counterfeit_notes', 'Counterfeit Notes', 50),
	('money_wash_dryera', 'Laundry Dryer A', 1),
	('money_wash_dryerb', 'Laundry Dryer B', 1),
	('money_wash_dryerc', 'Laundry Dryer C', 1),
	('money_wash_dryerd', 'Laundry Dryer D', 1),
	('money_wash_plates', 'Counterfeit Plates', 1),
	('doc_forge_equip_basic', 'Basic Forgery Equipment', 1),
	('doc_forge_equip_upgrade', 'Upgraded Forgery Equipment', 1),
	('doc_forge_interior_upgrade', 'Upgraded Forgery Interior', 1),
	('doc_forge_supplies', 'Document Forgery Supplies', 1),
	('pseudoephedrine', 'Pseudoephedrine', 1),
	('red_phosphorus', 'Red phosphorus', 1),
	('sodium_hydroxide', 'Sodium Hydroxide', 1),
	('sulfuric_acid', 'Sulfuric Acid', 1),
	('toluene', 'Toluene', 1),
	('lithium', 'Lithium', 1),
	('hydrochloric_acid', 'Hydrochloic Acid', 1),
	('anhydrous_ammonia', 'Anhydrous Ammonia', 1),
	('acetone', 'Acetone', 1)
;
