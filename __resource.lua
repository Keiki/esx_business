resource_manifest_version '44febabe-d386-4d18-afbe-5e627f4af937'

description 'ESX Business'

version '1.0.1'

server_scripts {
  '@es_extended/locale.lua',
  'locales/de.lua',
  'locales/br.lua',
  'locales/en.lua',
  'locales/fr.lua',
  'locales/es.lua',
  '@mysql-async/lib/MySQL.lua',
  'config.lua',
  'server/main.lua',
  'server/Cocaine_Lockup.lua',
  'server/Doc_Forge.lua',
  'server/Meth_Lab.lua',
  'server/Money_Wash.lua',
  'server/Weed_Farm.lua',
}

client_scripts {
  '@es_extended/locale.lua',
  'locales/de.lua',
  'locales/br.lua',
  'locales/en.lua',
  'locales/fr.lua',
  'locales/es.lua',
  'config.lua',
  'client/main.lua',
  'client/Cocaine_Lockup.lua',
  'client/Doc_Forge.lua',
  'client/Meth_Lab.lua',
  'client/Money_Wash.lua',
  'client/Weed_Farm.lua',
}

dependencies {
	'es_extended',
	'instance',
	'cron',
	'esx_addonaccount',
	'esx_addoninventory',
	'esx_datastore',
	'esx_jobs',
	'esx_policejob'
}
