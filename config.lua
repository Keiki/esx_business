Config                        = {}
Config.DrawDistance           = 100
Config.MarkerSize             = {x = 1.5, y = 1.5, z = 1.0}
Config.MarkerColor            = {r = 255, g = 255, b = 0}
Config.EnterExitMarkerColor   = {r = 102, g = 102, b = 204}
Config.RoomMenuMarkerColor    = {r = 102, g = 204, b = 102}
Config.MarkerType             = 1
Config.Zones                  = {}
Config.Businesses             = {}
Config.EnablePlayerManagement = false -- If set to true you need esx_realestateagentjob
Config.Locale                 = 'en'
Config.TimeToProcess          = 30 --time in seconds
Config.TimeToAnim             = 10 --time in seconds
Config.TimeToExtract          = 30 --time in seconds
Config.TimeToGrowWeedBasic    = 30 --time in mins
Config.TimeToGrowWeedUpgrade  = 30 --time in mins
Config.TimeToDryWeed          = 60 --time in mins
Config.TimeToPrintBasic       = 60 --time in mins
Config.TimeToPrintUpgrade     = 60 --time in mins
Config.TimeToCookMethBasic    = 60 --time in mins
Config.TimeToCookMethUpgrade  = 60 --time in mins
Config.TimeToProcessMeth      = 60 --time in seconds
Config.TimeToProcessCoke      = 60 --time in seconds
Config.TimeToProcessWeed      = 60 --time in seconds
Config.BlackMoneyConversionFactor = 0.8
Config.GiveBlackMoneyAmount   = 250000
Config.HarvestWeedAmountBasic = 9
Config.HarvestWeedAmountUpgrade = 18
Config.GiveCounterfeitNotesBasic = 1
Config.GiveCounterfeitNotesUpgrade = 2
Config.GiveUnprocessedMethBasic = 10
Config.GiveUnprocessedMethUpgrade = 20


Config.BusinessSetup = {
  Weed_Farm = {
    type = "Weed_Farm",
    interiorID = 247297,
    planterLetters = {'a','b','c','d','e','f','g','h'},
    planterVars = {'planter_a','planter_b','planter_c','planter_d','planter_e','planter_f','planter_g','planter_h'},
    planterSetupVars = {'setup_planter_a','setup_planter_b','setup_planter_c','setup_planter_d','setup_planter_e','setup_planter_f','setup_planter_g','setup_planter_h'},
    planterHarvestVars = {'harvest_planter_a','harvest_planter_b','harvest_planter_c','harvest_planter_d','harvest_planter_e','harvest_planter_f','harvest_planter_g','harvest_planter_h'},
    props = {
      --"weed_chairs",
      "weed_drying",
      "weed_production",
      "weed_set_up",
      "weed_upgrade_equip",
      "weed_hosea",
      "weed_hoseb",
      "weed_hosec",
      "weed_hosed",
      "weed_hosee",
      "weed_hosef",
      "weed_hoseg",
      "weed_hoseh",
      "weed_hosei",
      "weed_growtha_stage1",
      "weed_growtha_stage2",
      "weed_growtha_stage3",
      "weed_growthb_stage1",
      "weed_growthb_stage2",
      "weed_growthb_stage3",
      "weed_growthc_stage1",
      "weed_growthc_stage2",
      "weed_growthc_stage3",
      "weed_growthd_stage1",
      "weed_growthd_stage2",
      "weed_growthd_stage3",
      "weed_growthe_stage1",
      "weed_growthe_stage2",
      "weed_growthe_stage3",
      "weed_growthf_stage1",
      "weed_growthf_stage2",
      "weed_growthf_stage3",
      "weed_growthg_stage1",
      "weed_growthg_stage2",
      "weed_growthg_stage3",
      "weed_growthh_stage1",
      "weed_growthh_stage2",
      "weed_growthh_stage3",
      "weed_growthi_stage1",
      "weed_growthi_stage2",
      "weed_growthi_stage3"
    },
    zones = {
      planters = {
        {x=1051.552, y=-3195.823, z=-40.09},
        {x=1051.522, y=-3190.166, z=-40.072},
        {x=1056.11, y=-3189.964, z=-40.067},
        {x=1063.179, y=-3193.235, z=-40.083},
        {x=1063.193, y=-3198.054, z=-40.051},
        {x=1063.453, y=-3204.318, z=-40.09},
        {x=1057.657, y=-3205.859, z=-40.077},
        {x=1057.599, y=-3199.774, z=-40.067}
      },
      drying = {x=1040.81,y=-3202.33,z=-39.16},
      production = {
        {x=1039.375,y=-3205.951,z=-39.16},
        {x=1037.224,y=-3205.951,z=-39.16},
        {x=1034.785,y=-3206.146,z=-39.16},
        {x=1032.633,y=-3206.146,z=-39.16}
      },
      exit = {x=1066.33,y=-3183.52,z=-40.114},
      inside = {y=-3184.765,z=-40.114,x=1062.564},
      roomMenu = {x=1044.5,y=-3194.94,z=-39.16}
    },
    --IPLs = {"bkr_biker_interior_placement_interior_3_biker_dlc_int_ware02_milo"},
    price = 250000
    --price = 0
  },

  Cocaine_Lockup = {
    type = "Cocaine_Lockup",
    interiorID = 247553,
    cokeNumbers = {'01','02','03','04','05'},
    cokeVars = {'coke_01','coke_02','coke_03','coke_04','coke_05'},
    cokeSetupVars = {'setup_coke_01','setup_coke_02','setup_coke_03','setup_coke_04','setup_coke_05'},
    --cokeTakeVars = {'take_coke_01','take_coke_02','take_coke_03','take_coke_04','take_coke_05'},
    props = {
      --'equipment_basic',
      'equipment_upgrade',
      'coke_press_basic',
      'production_basic',
      'table_equipment',
      'coke_press_upgrade',
      'production_upgrade',
      'table_equipment_upgrade',
      --'security_low',
      --'security_high',
      'coke_cut_01',
      'coke_cut_02',
      'coke_cut_03',
      'coke_cut_04',
      'coke_cut_05'
    },
    zones = {
      production = {
        {x=1090.28,y=-3196.69,z=-40.16},
        {x=1092.84,y=-3194.89,z=-40.16},
        {x=1095.31,y=-3194.83,z=-40.16}
      },
      production_upgrade = {
        {x=1099.57,y=-3194.20,z=-40.16},
        {x=1101.99,y=-3193.77,z=-40.16}
      },
      exit = {x=1088.799,y=-3188.179,z=-39.943},
      inside = {y=-3190.751,z=-39.943,x=1088.544},
      roomMenu = {x=1087.27,y=-3194.26,z=-40.16}
    },
    --IPLs = {"bkr_biker_interior_placement_interior_4_biker_dlc_int_ware03_milo"},
    price = 500000
    --price = 0
  },

  Meth_Lab = {
    type = "Meth_Lab",
    interiorID = 247041,
    props = {
      'meth_lab_basic',
      'meth_lab_upgrade',
      'meth_lab_production',
      'meth_lab_security_high'
    },
    zones = {
      production = {x=1005.81,y=-3200.36,z=-39.52},
      processing = {x=1014.95,y=-3194.91,z=-39.99},
      exit = {x=997.54,y=-3200.711,z=-37.344},
      inside = {y=-3195.851,z=-38.344,x=998.017},
      roomMenu = {x=1001.87,y=-3194.96,z=-40.16}
    },
    --IPLs = {"bkr_biker_interior_placement_interior_2_biker_dlc_int_ware01_milo"},
    price = 750000
    --price = 0
  },

  Doc_Forge = {
    type = "Doc_Forge",
    interiorID = 246785,
    props = {
      "interior_basic",
      "interior_upgrade",
      "equipment_basic",
      "equipment_upgrade",
      "security_low",
      "security_high",
      "production",
      "set_up"
      --"clutter"
    },
    zones = {
      production = {
        {x=1167.173,y=-3196.401,z=40.01},
        {x=1165.655,y=-3197.064,z=40.01},
        {x=1159.19,y=-3195.834,z=40.01},
        {x=1157.964,y=-3198.534,z=40.01}
      },
      exit = {x=1173.69,y=-3196.61,z=-40.344},
      inside = {y=-3196.55,z=-40.01,x=1171.91},
      roomMenu = {x=1160.33,y=-3192.28,z=-40.01}
    },
    --IPLs = {"bkr_biker_interior_placement_interior_6_biker_dlc_int_ware05_milo"},
    price = 500000
    --price = 0
  },

  Money_Wash = {
    type = "Money_Wash",
    interiorID = 247809,
    dryerLetters = {'a','b','c','d'},
    dryerVars = {'dryer_a','dryer_b','dryer_c','dryer_d'},
    dryerSetupVars = {'setup_dryer_a','setup_dryer_b','setup_dryer_c','setup_dryer_d'},
    dryerTakeVars = {'take_dryer_a','take_dryer_b','take_dryer_c','take_dryer_d'},
    props = {
      "counterfeit_standard_equip_no_prod",
      "counterfeit_upgrade_equip_no_prod",
      "counterfeit_standard_equip",
      "counterfeit_upgrade_equip",
      --"counterfeit_low_security",
      --"counterfeit_security",
      "dryera_on",
      "dryera_off",
      "dryera_open",
      "dryerb_on",
      "dryerb_off",
      "dryerb_open",
      "dryerc_on",
      "dryerc_off",
      "dryerc_open",
      "dryerd_on",
      "dryerd_off",
      "dryerd_open",
      "counterfeit_setup"
    },
    zones = {
      production = {
        {x=1120.115,y=-3197.883,z=-41.4},
        {x=1117.817,y=-3197.006,z=-41.4}
      },
      dryers = {
        {x=1122.43, y=-3194.42, z=-41.4},
        {x=1123.82, y=-3194.35, z=-41.4},
        {x=1125.48, y=-3194.27, z=-41.4},
        {x=1127.00, y=-3194.3, z=-41.4}
      },
      printerStartBasic = {x=1133.2,y=-3197.19,z=-40.67},
      printerStartUpgrade = {x=1136.1,y=-3197.07,z=-40.67},
      printerTake = {x=1126.21,y=-3197.09,z=-40.67},
      exit = {x=1138.02,y=-3199.13,z=-40.67},
      inside = {y=-3196.87,z=-40.67,x=1136.36},
      roomMenu = {x=1129.49,y=-3194.15,z=-41.4}
    },
    price = 1000000
    --price = 0
  }
}

Config.HeistZones = {
  chemicalPlant = {
    {x=2659.26,y=1615.76,z=34.32},
    {x=2665.61,y=1563.67,z=34.28},
    {x=2666.92,y=1546.88,z=34.31},
    {x=2664.59,y=1496.72,z=34.31},
    {x=2665.59,y=1478.13,z=34.27},
    {x=2659.36,y=1382.51,z=33.64},
    {x=2659.38,y=1357.53,z=33.83},
    {x=2706.43,y=1451.83,z=34.18},
    {x=2720.02,y=1445.98,z=34.19}
  }
}

Config.Blips = {
	ChemicalPlant = {coords = vector3(2661.61, 1642.04, 24.87), name = _U('blip_chemicals'), color = 6, sprite = 499},
}
